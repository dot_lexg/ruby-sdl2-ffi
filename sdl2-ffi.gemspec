# frozen_string_literal: true

require_relative 'lib/sdl2/wrapper_version'

Gem::Specification.new do |spec|
	spec.name = 'sdl2-ffi'
	spec.required_ruby_version = '>= 2.7'
	spec.version = SDL2::WRAPPER_VERSION
	spec.summary = 'SDL2 FFI'
	spec.description = <<-DESCRIPTION
		A ruby FFI wrapper for the SDL2 library.

		Simple DirectMedia Layer is a cross-platform development library designed to
		provide low level access to audio, keyboard, mouse, joystick, and graphics
		hardware via OpenGL and Direct3D.

		See: https://www.libsdl.org/
		DESCRIPTION
	spec.license = 'GPL-3.0'
	spec.authors = ['Alex Gittemeier']
	spec.email = 'gittemeier.alex@protonmail.com'
	spec.homepage = 'https://github.com/win93/ruby-sdl2-ffi'
	spec.files = `git ls-files`.split(/\n/)
	spec.test_files = []

	spec.add_dependency 'ffi', '~> 1.13'
	spec.add_development_dependency 'pry', '~> 0.14.1'
end
