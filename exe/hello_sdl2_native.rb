#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/sdl2/native'

SDL2::Native.check(:init, [:video, :events])
window = SDL2::Native.check(:create_window, 'Demo Game', SDL2::Native::WINDOWPOS_UNDEFINED, SDL2::Native::WINDOWPOS_UNDEFINED, 640, 480, :opengl)
renderer = SDL2::Native.check(:create_renderer, window, -1, :accelerated)

at_exit do
	SDL2::Native.destroy_renderer(renderer)
	SDL2::Native.destroy_window(window)
	SDL2::Native.quit()
end

event = SDL2::Native::Event.new
loop do
	while SDL2::Native.poll_event(event) != 0
		case event[:type]
		when :quit
			exit 0
		end
	end


	red = rand(256)
	green = rand(256)
	blue = rand(256)

	# renderer.draw_color = 0xFF7F00_FF # should be allowed to do it either way
	SDL2::Native.check(:set_render_draw_color, renderer, red, green, blue, 255)
	SDL2::Native.check(:render_clear, renderer)
	SDL2::Native.render_present(renderer)
end
