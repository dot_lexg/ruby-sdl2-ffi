#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/sdl2'
require_relative '../lib/sdl2/ttf'
require_relative '../lib/sdl2/image'
require_relative '../lib/tracer'


Tracer.trace_module(SDL2::Native, ignore: [:check, :require_value_type])
Tracer.trace_module(SDL2::TTF::Native, ignore: [:check])
Tracer.trace_module(SDL2::Image::Native, ignore: [:check])


SDL2.init

window = SDL2::Window.new('Demo Game', nil, nil, 640, 480)
renderer = window.new_renderer(:accelerated, :presentvsync)
font = SDL2::TTF::Font.new('/Users/alexg/Library/Fonts/JetBrainsMono-Regular.ttf', 48)

(0..).each do |i|
  $stderr.puts "frame #{i} #{'-' * 80}"

	while (event = SDL2::Event.poll)
		p event
		case event.type
		when :quit
			exit
		when :keydown
			exit if event.keysym.scancode == :ESCAPE
		end
	end

	renderer.draw_color = 0xFF7F00_FF
	renderer.clear

	ss, ff = i.divmod(75)
	mm, ss = ss.divmod(60)
	hh, mm = mm.divmod(60)

	surface = font.render_blended("%d:%02d'%02d\" %02d%s" % [hh, mm, ss, ff, ['A ', ' B'][ff % 2]], fg: 0xFFFFFF_FF)
	renderer.copy(surface.to_texture(renderer), dst: [0, 0])

	renderer.present
end
