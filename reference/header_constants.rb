#!/usr/bin/env ruby
# frozen_string_literal: true

while (line = gets&.chomp)
	while line[-1] == '\\'
		line[-1] = ''
		line << gets&.chomp
	end

	while (comment_start = line.index('/*'))
		line << gets&.chomp until (comment_end = line.index('*/'))
		while line[-1] == '\\'
			line[-1] = ''
			line << gets&.chomp
		end
		line[comment_start..comment_end + 1] = ''
	end
	line.strip!

	next if !(m = line.match(/\A^\s*#\s*define\s+(\w*)\s+(.*?)\s*\z/))

	name, value = m.captures
	value.gsub!(/\s+/, ' ')
	puts "#{name} = #{value}"
end
