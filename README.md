# Ruby FFI bindings for SDL2

- [Gem Documentation](https://windows93.gitlab.io/ruby-sdl2-ffi/)
- [Native FFI Documentation](https://windows93.gitlab.io/ruby-sdl2-ffi/native/)


## Setup for Windows

This gem depends on SDL2.dll and friends to be installed somewhere on disk. You must point to the folder where these DLLs are located via the environment variable RUBY_DLL_PATH. Perhaps we could embed these into a gem...

### Download links
- https://www.libsdl.org/download-2.0.php (in the center of the page, choose Runtime Binaries > Windows > 64-bit Windows)
- https://www.libsdl.org/projects/SDL_image/  (choose Runtime Binaries > Windows > 64-bit Windows)
- https://www.libsdl.org/projects/SDL_ttf/ (choose Runtime Binaries > Windows > 64-bit Windows)

Extract each one of these and put the DLLs into one folder. `zlib1.dll` exists in two different .zip files, they are very likely the same DLL, so you only need one of them.

### Set up environment variable

- https://github.com/oneclick/rubyinstaller2/wiki/For-gem-developers#-dll-loading
