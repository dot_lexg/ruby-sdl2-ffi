# frozen_string_literal: true

require_relative 'native/version'

module SDL2
	TARGET_VERSION = Native::TARGET_VERSION
	LINKED_VERSION = begin
		version = Native::Version.new
		Native.get_version(version)
		"#{version[:major]}.#{version[:minor]}.#{version[:patch]}"
	end
	LINKED_BUILDINFO = "#{LINKED_VERSION} (#{Native.get_revision})"
end
