# frozen_string_literal: true

module SDL2
  class Vector
    def initialize(*components)
      @components = components
    end

    def to_a = @components
    def length = @components.length
    def [](index) = @components[index]

    def x
      raise NoMethodError unless length >= 1

      @components[0]
    end

    def y
      raise NoMethodError unless length >= 2

      @components[1]
    end

    def z
      raise NoMethodError unless length >= 3

      @components[2]
    end

    def w
      raise NoMethodError unless length >= 4

      @components[3]
    end


    def +(other) = dup.add!(other)
    def -(other) = dup.subtract!(other)
    def *(scalar) = dup.scale!(scalar)

    def []=(index, value)
      @components[index] = value
    end


    def x=(value)
      raise NoMethodError unless length >= 1

      @components[0] = value
    end

    def y=(value)
      raise NoMethodError unless length >= 2

      @components[1] = value
    end

    def z=(value)
      raise NoMethodError unless length >= 3

      @components[2] = value
    end

    def w=(value)
      raise NoMethodError unless length >= 4

      @components[3] = value
    end

    def add!(other)
      raise ArgumentError, "Cannot add #{length}-vector and #{other.length}-vector" unless length == other.length

      @components.map!.with_index do |component, i|
        component + other[i]
      end
      self
    end

    def subtract!(other)
      raise ArgumentError, "Cannot subtract #{length}-vector and #{other.length}-vector" unless length == other.length

      @components.map!.with_index do |component, i|
        component - other[i]
      end
      self
    end

    def scale!(scalar)
      @components.map! do |component|
        component * scalar
      end
      self
    end
  end
end
