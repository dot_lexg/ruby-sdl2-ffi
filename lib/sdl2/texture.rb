# frozen_string_literal: true

require_relative 'native'

module SDL2
	# XXX: this class should not be managed with a garbage collector because each reference represents a hardware texture, a scarce resource on some targets.
	# TODO: determine how to query for the texture limit on a target, and emit a warning/error if the limit is reached (SDL doesn't do this error checking for us, instead textures are silently filled to black)
	class Texture
		def self.from_surface(renderer, surface)
			wrap_native(FFI::AutoPointer.new(
				Native.check(:create_texture_from_surface, renderer.native, surface.native),
				Native.method(:destroy_texture)
			))
		end

		def self.wrap_native(native)
			texture = allocate
			texture.initialize_wrapped(native)
			texture
		end

		def initialize(renderer, format, access, width, height)
			Native.require_value_type(renderer, Renderer)

			@native = FFI::AutoPointer.new(
				Native.check(:create_texture, renderer.native, format, access, width, height),
				Native.method(:destroy_texture)
			)
			@format = format
			@access = access
			@width = width
			@height = height
		end

		def initialize_wrapped(native)
			format = Native::PixelFormatEnumPtr.new
			access = Native::IntPtr.new # TODO: use SDL_TextureAccess enum?
			width = Native::IntPtr.new
			height = Native::IntPtr.new
			Native.check(:query_texture, native, format, access, width, height)

			@native = native
			@format = format
			@access = access.value
			@width = width.value
			@height = height.value
		end

		attr_reader :native, :width, :height

		# the unit for pitch is array elements, not bytes as in the C api
		def update(pixels, pitch = nil)
			elements_per_pixel, element_type = pixel_array_layout(@format)
			array_size = @width * @height * elements_per_pixel
			raise "Expected array/string of length #{array_size}" if pixels.length != array_size && element_type == FFI::TYPE_UINT8
			raise "Expected array of length #{array_size}" if pixels.length != array_size

			pixel_buffer = FFI::MemoryPointer.new(element_type, array_size)
			case element_type
			when FFI::TYPE_UINT8
				if pixels.is_a?(String)
					pixel_buffer.write(pixels)
				else
					pixel_buffer.write_array_of_uint8(pixels)
				end
			when FFI::TYPE_UINT16
				pixel_buffer.write_array_of_uint16(pixels)
			when FFI::TYPE_UINT32
				pixel_buffer.write_array_of_uint32(pixels)
			end

			pitch ||= @width * elements_per_pixel
			pitch *= FFI.type_size(element_type)

			Native.check(:update_texture, @native, nil, pixel_buffer, pitch)
			nil
		end

		def lock(rect = nil)
			pixels_ptr = FFI::MemoryPointer.new(:pointer)
			pitch_ptr = FFI::MemoryPointer.new(:int)
			rect &&= Native.Rect(rect)

			Native.check(:lock_texture, @native, rect, pixels_ptr, pitch_ptr)
			[pixels_ptr.read_pointer, pitch_ptr.read_int]
		end

		def unlock
			Native.unlock_texture(@native)
		end

		def gc
			@native.free
			@native = nil
		end

		private

		# TODO: should probably me moved to a pixel buffer class
		def pixel_array_layout(pixel_format) # => elements_per_pixel, element_type
			if Native.pixelformat_array?(pixel_format)
				[Native.bytes_per_pixel(pixel_format), FFI::TYPE_UINT8]
			else
				case Native.bytes_per_pixel(pixel_format)
				when 1
					[1, FFI::TYPE_UINT8]
				when 2
					[1, FFI::TYPE_UINT16]
				when 4
					[1, FFI::TYPE_UINT32]
				else
					raise "Invalid pixel format: #{pixel_format.inspect}"
				end
			end
		end
	end
end
