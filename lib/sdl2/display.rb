# frozen_string_literal: true

require_relative 'native'

module SDL2
	DisplayMode = Struct.new(:format, :width, :height, :refresh_rate) do
		def self.from_native(native)
			new(native[:format], native[:w], native[:h], native[:refresh_rate])
		end

		def to_native
			native = Native::DisplayMode.new
			native[:format] = format
			native[:w] = width
			native[:h] = height
			native[:refresh_rate] = refresh_rate
			native[:driverdata] = FFI::Pointer::NULL
			native
		end
	end

	class Display
		DPI = Struct.new(:diagonal, :horizontal, :vertical)

		def self.all
			count = Native.check(:get_num_video_displays)
			count.times.map do |index|
				new(index)
			end
		end

		# TODO: can memoize our instances because state is frozen and there's a limited number of possible instances
		def self.[](index)
			new(index)
		end

		def initialize(index)
			@index = index
			freeze
		end

		attr_reader :index

		# TODO: optionally allow DisplayMode struct to be passed in
		def closest_mode(format: 0, width:, height:, refresh_rate: 0)
			in_mode = Native::DisplayMode.new
			in_mode[:format] = format
			in_mode[:w] = width
			in_mode[:h] = height
			in_mode[:refresh_rate] = refresh_rate
			out_mode = Native::DisplayMode.new
			Native.check(:get_closest_display_mode, @index, in_mode, out_mode)
			DisplayMode.from_native(out_mode)
		end

		def current_mode
			mode = Native::DisplayMode.new
			Native.check(:get_current_display_mode, @index, mode)
			DisplayMode.from_native(mode)
		end

		def desktop_mode
			mode = Native::DisplayMode.new
			Native.check(:get_desktop_display_mode, @index, mode)
			DisplayMode.from_native(mode)
		end

		def bounds
			rect = Native::Rect.new
			Native.check(:get_display_bounds, @index, rect)
			Rect.from_native(rect)
		end

		def dpi
			ddpi = Native::FloatPtr.new
			hdpi = Native::FloatPtr.new
			vdpi = Native::FloatPtr.new
			Native.check(:get_display_dpi, @index, ddpi, hdpi, vdpi)
			DPI.new(ddpi.value, hdpi.value, vdpi.value)
		end

		def modes
			count = Native.check(:get_num_display_modes, @index)
			count.times.map do |i|
				mode = Native::DisplayMode.new
				Native.check(:get_display_mode, @index, i, mode)
				DisplayMode.from_native(mode)
			end
		end

		def name
			Native.get_display_name(@index).force_encoding('UTF-8')
		end

		def usable_bounds
			rect = Native::Rect.new
			Native.check(:get_display_usable_bounds, @index, rect)
			Rect.from_native(rect)
		end

		def inspect
			<<~INSPECT
				#<#{self.class}[#{@index}] name: #{name.inspect}
				current_mode: #{current_mode.inspect} desktop_mode: #{desktop_mode.inspect}
				bounds: #{bounds.inspect} usable_bounds: #{usable_bounds.inspect}
				dpi: #{dpi.inspect} modes: [ length: #{modes.length} ]>"
			INSPECT
		end
	end
end
