# frozen_string_literal: true

require_relative 'native'
require_relative 'display'
module SDL2
	class Window
		Insets = Struct.new(:top, :left, :bottom, :right)

		@@windows_by_id = [] # TODO: consider using SetWindowData to go from native to wrapper?

		def self.grabbed_window
			native = Native.get_grabbed_window
			return nil if window == FFI::Pointer::NULL

			Window[Native.check(:get_window_id, native)]
		end

		def self.[](window_id)
			window = @@windows_by_id[window_id]
			raise if window.id != window_id

			window
		end

		# TODO: potentially allow not specifiying x and y
		def initialize(title, x, y, width, height, *flags)
			x = Native::WINDOWPOS_CENTERED if x == :center
			x ||= Native::WINDOWPOS_UNDEFINED
			x = Native::WINDOWPOS_CENTERED if x == :center
			y ||= Native::WINDOWPOS_UNDEFINED

			flags << :fullscreen if flags.include?(:fullscreen_desktop)

			@native = Native.check(:create_window, title, x, y, width, height, flags)
			@tag = nil
			@@windows_by_id[self.id] = self
		end

		attr_reader :native
		attr_accessor :tag

		def new_renderer(*flags, index: -1)
			Renderer.new(self, *flags, index: index)
		end

		def hit_test
			raise ArgumentError, 'must provide a block' if !block_given?

			callback = proc do |window, point|
				raise if window != @native

				yield(*point.values)
			end

			Native.check(:set_window_hit_test, @native, callback, nil)
			nil
		end

		def destroy
			Native.destroy_window(@native)
			@native = nil
		end

		def destroyed?
			!@native
		end

		def hide
			Native.hide_window(@native)
		end

		def maximize
			Native.maximize_window(@native)
		end

		def modal_for(other_window)
			Native.check(:set_window_modal_for, @native, other_window.native)
		end

		def minimize
			Native.minimize_window(@native)
		end

		# not called +raise+ to avoid conflict with Kernel.raise
		def focus
			Native.raise_window(@native)
		end

		# You probably want {#focus}
		def input_focus
			Native.check(:set_window_input_focus, @native)
		end

		def restore
			Native.restore_window(@native)
		end

		def show
			Native.show_window(@native)
		end

		def update_surface
			Native.check(:update_window_surface, @native)
			nil
		end

		def bordered=(bordered)
			Native.set_window_bordered(@native, bordered)
		end

		def bordered?
			!self.flags.include?(:borderless)
		end

		# TODO: test this (not supported on macOS)
		def borders_size
			top = Native::IntPtr.new
			left = Native::IntPtr.new
			bottom = Native::IntPtr.new
			right = Native::IntPtr.new
			Native.check(:get_window_borders_size, @native, top, left, bottom, right)
			Insets.new(top.value, left.value, bottom.value, right.value)
		end

		def display
			SDL2::Display[Native.check(:get_window_display_index, @native)]
		end

		def display_brightness
			Native.get_window_brightness(@native)
		end

		def display_brightness=(brightness)
			Native.set_window_brightness(@native, brightness)
		end

		#TODO: this can also be set
		def display_gamma_ramp
			red = FFI::MemoryPointer.new(FFI::TYPE_UINT16, 256)
			green = FFI::MemoryPointer.new(FFI::TYPE_UINT16, 256)
			blue = FFI::MemoryPointer.new(FFI::TYPE_UINT16, 256)
			Native.check(:get_window_gamma_ramp, @native, red, green, blue)
			[red.read_array_of_uint16(256), green.read_array_of_uint16(256), blue.read_array_of_uint16(256)]
		end

		def flags
			Native.get_window_flags(@native)
		end

		def fullscreen_mode
			return :fullscreen_desktop if self.flags.include?(:fullscreen_desktop)
			return :fullscreen if self.flags.include?(:fullscreen)

			:windowed
		end

		def fullscreen?
			self.flags.include?(:fullscreen)
		end

		#TODO: this is kind of a jank API, consider revising
		def fullscreen_mode=(fullscreen)
			fullscreen = :fullscreen if fullscreen == true
			Native.check(:set_window_fullscreen, @native, fullscreen)
		end

		def fullscreen_display_mode
			mode = Native::DisplayMode.new
			Native.check(:get_window_display_mode, @native, mode)
			DisplayMode.from_native(mode)
		end

		def fullscreen_display_mode=(mode)
			Native.check(:set_window_display_mode, @native, mode.to_native)
		end

		def input_grabbed?
			Native.get_window_grab(@native)
		end

		def grab_input=(grab)
			Native.set_window_grab(@native, grab)
		end

		def icon=(icon)
			Native.set_window_icon(@native, icon)
		end

		def id
			Native.check(:get_window_id, @native)
		end

		def maximum_size
			width = Native::IntPtr.new
			height = Native::IntPtr.new
			Native.get_window_maximum_size(@native, width, height)
			[width.value, height.value]
		end

		def maximum_size=((width, height))
			Native.set_window_maximum_size(@native, width, height)
		end

		def minimum_size
			width = Native::IntPtr.new
			height = Native::IntPtr.new
			Native.get_window_minimum_size(@native, width, height)
			[width.value, height.value]
		end

		def minimum_size=((width, height))
			Native.set_window_minimum_size(@native, width, height)
		end

		def opacity
			opacity = Native::FloatPtr.new
			Native.check(:get_window_opacity, @native, opacity)
			opacity.value
		end

		def opacity=(opacity)
			Native.check(:set_window_opacity, @native, opacity)
		end

		def pixel_format
			Native.check(:get_window_pixel_format, @native)
		end

		def position
			x = Native::IntPtr.new
			y = Native::IntPtr.new
			Native.get_window_position(@native, x, y)
			[x.value, y.value]
		end

		def position=((x, y))
			Native.set_window_position(@native, x, y)
		end

		def resizable?
			self.flags.include?(:resizable)
		end

		def resizable=(resizable)
			Native.set_window_resizable(@native, resizable)
		end

		def size
			width = Native::IntPtr.new
			height = Native::IntPtr.new
			Native.get_window_size(@native, width, height)
			[width.value, height.value]
		end

		def size=((width, height))
			Native.set_window_size(@native, width, height)
		end

		def surface
			Native.check(:get_window_surface, @native) # TODO: wrap
		end

		def title
			Native.get_window_title(@native).force_encoding('UTF-8')
		end

		def title=(title)
			Native.set_window_title(@native, title.encode('UTF-8'))
		end

		def inspect
			return "#<#{self.class} (destroyed)>" if destroyed?

			<<~INSPECT
				#<#{self.class} id=#{id.inspect} title=#{title.inspect} flags=#{flags.inspect} tag=#{tag.inspect}
					display.index=#{display.index.inspect} position=#{position.inspect} input_grabbed=#{input_grabbed?}
					pixel_format=#{pixel_format.inspect} opacity=#{opacity.inspect}
					size=#{size.inspect} minimum_size=#{minimum_size.inspect} maximum_size=#{maximum_size.inspect}>
			INSPECT
		end
	end
end
