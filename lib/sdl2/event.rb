# frozen_string_literal: true

require_relative 'native'

module SDL2
	class Event
		class << self
			def poll
				@event ||= Native::Event.new
				return if Native.poll_event(@event) == 0

				resolved_event = @event.resolve_type
				if (clazz = CLASSES[resolved_event.class])
					clazz.new(*resolved_event.values)
				else
					Unknown.new(*@event.values)
				end
			end

			def each
				while (event = poll)
					yield event
				end
			end

			# returns true if this toggled the status, false if it did nothing
			def enable(type)
				Native.event_state(type, :enable) == :disable
			end

			# returns true if this toggled the status, false if it did nothing
			def disable(type)
				Native.event_state(type, :disable) == :enable
			end
			alias ignore disable

			def enabled?(type)
				Native.event_state(type, :query) == :enable
			end
		end

		def initialize(type, timestamp)
			@type = type
			@timestamp = timestamp
		end

		attr_reader :type, :timestamp
		class Unknown < Event
			def initialize(type, timestamp, data)
				super(type, timestamp)
				@data = data.to_a.map {|b| b < 0 ? (b + 256).chr : b.chr }.join # TODO: somehow figure out if this works, then delete the rescue below
			rescue
				warn "type=#{type.inspect}, timestamp=#{timestamp.inspect}, data=#{data.inspect}"
				raise
			end

			attr_reader :data
		end

		class AudioDevice < Event
			def initialize(type, timestamp, which, iscapture)
				super(type, timestamp)
				@which = which
				@iscapture = iscapture != 0
			end
			attr_reader :which, :iscapture
			alias capture? iscapture
		end

		class ControllerAxis < Event ###
			def initialize(type, timestamp, which, axis, value)
				super(type, timestamp)
				@which = which
				@axis = axis
				@value = value
			end

			attr_reader :which, :axis, :value
		end

		class ControllerButton < Event ###
			def initialize(type, timestamp, which, button, state)
				super(type, timestamp)
				@which = which
				@button = button
				@state = state
			end

			attr_reader :which, :button, :state
		end

		class ControllerDevice < Event ###
			def initialize(type, timestamp, which)
				super(type, timestamp)
				@which = which
			end

			attr_reader :which
		end

		class DollarGesture < Event ###
			def initialize(type, timestamp, touch_id, gesture_id, num_fingers, error, x, y)
				super(type, timestamp)
				@touch_id = touch_id
				@gesture_id = gesture_id
				@num_fingers = num_fingers
				@error = error
				@pos = Vector.new(x, y)
			end
			attr_reader :touch_id, :gesture_id, :num_fingers, :error, :pos

			def x = pos.x
			def y = pos.y
		end

		class Drop < Event ###
			def initialize(type, timestamp, file, window_id)
				super(type, timestamp)
				@file = file
				@window_id = window_id
			end

			attr_reader :file, :window_id
		end

		class JoyAxis < Event ###
			def initialize(type, timestamp, which, axis, value)
				super(type, timestamp)
				@which = which
				@axis = axis
				@value = value
			end

			attr_reader :which, :axis, :value
		end

		class JoyBall < Event ###
			def initialize(type, timestamp, which, ball, xrel, yrel)
				super(type, timestamp)
				@which = which
				@ball = ball
				@xrel = xrel
				@yrel = yrel
			end

			attr_reader :which, :ball, :xrel, :yrel
		end

		class JoyButton < Event ###
			def initialize(type, timestamp, which, button, state)
				super(type, timestamp)
				@which = which
				@button = button
				@state = state
			end

			attr_reader :which, :button, :state
		end

		class JoyDevice < Event ###
			def initialize(type, timestamp, which)
				super(type, timestamp)
				@which = which
			end

			attr_reader :which
		end

		class JoyHat < Event ###
			def initialize(type, timestamp, which, hat, value)
				super(type, timestamp)
				@which = which
				@hat = hat
				@value = value
			end

			attr_reader :which, :hat, :value
		end

		Keysym = Struct.new(:scancode, :sym, :mod)
		class Keyboard < Event
			def initialize(type, timestamp, window_id, state, repeat, keysym)
				super(type, timestamp)
				@window_id = window_id
				@state = state
				@repeat = repeat != 0
				@keysym = Keysym.new(keysym[:scancode], keysym[:sym], keysym[:mod]).freeze
			end

			attr_reader :window_id, :state, :repeat, :keysym
			alias repeat? repeat
		end

		class MouseButton < Event
			def initialize(type, timestamp, window_id, which, button, state, clicks, x, y)
				super(type, timestamp)
				@window_id = window_id
				@which = (which == Native::TOUCH_MOUSEID ? :touch : which)
				@button = button
				@state = state
				@clicks = clicks
				@pos = Vector.new(x, y)
			end
			attr_reader :window_id, :which, :button, :state, :clicks, :pos

			def x = pos.x
			def y = pos.y
		end

		class MouseMotion < Event
			def initialize(type, timestamp, window_id, which, state, x, y, xrel, yrel)
				super(type, timestamp)
				@window_id = window_id
				@which = (which == Native::TOUCH_MOUSEID ? :touch : which)
				@state = state
				@pos = Vector.new(x, y)
				@rel = Vector.new(xrel, yrel)
			end
			attr_reader :window_id, :which, :state, :pos, :rel

			def x = pos.x
			def y = pos.y
			def xrel = rel.x
			def yrel = rel.y
		end

		class MouseWheel < Event
			def initialize(type, timestamp, window_id, which, x, y, direction)
				super(type, timestamp)
				@window_id = window_id
				@which = (which == Native::TOUCH_MOUSEID ? :touch : which)
				@x = x
				@y = y
				@direction = direction
			end

			attr_reader :window_id, :which, :x, :y, :direction
		end

		class MultiGesture < Event ###
			def initialize(type, timestamp, touch_id, d_theta, d_dist, x, y, num_fingers)
				super(type, timestamp)
				@touch_id = touch_id
				@d_theta = d_theta
				@d_dist = d_dist
				@x = x
				@y = y
				@num_fingers = num_fingers
			end

			attr_reader :touch_id, :d_theta, :d_dist, :x, :y, :num_fingers
		end

		class Quit < Event; end

		class SysWM < Event ###
			def initialize(type, timestamp, msg)
				super(type, timestamp)
				@msg = msg
			end

			attr_reader :msg
		end

		class TextEditing < Event
			def initialize(type, timestamp, window_id, text, start, length)
				super(type, timestamp)
				@window_id = window_id
				@text = text.to_s.force_encoding('UTF-8')
				@start = start
				@length = length
			end

			attr_reader :window_id, :text, :start, :length
		end

		class TextInput < Event
			def initialize(type, timestamp, window_id, text)
				super(type, timestamp)
				@window_id = window_id
				@text = text.to_s.force_encoding('UTF-8')
			end

			attr_reader :window_id, :text
		end

		class TouchFinger < Event ###
			def initialize(type, timestamp, touch_id, finger_id, x, y, endx, endy, pressure, window_id)
				super(type, timestamp)
				@touch_id = touch_id
				@finger_id = finger_id
				@start = Vector.new(x, y)
				@end = Vector.new(endx, endy)
				@pressure = pressure
				@window_id = window_id
			end

			attr_reader :touch_id, :finger_id, :start, :end, :pressure, :window_id
		end

		class User < Event ###
			def initialize(type, timestamp, window_id, code, data1, data2)
				super(type, timestamp)
				@window_id = window_id
				@code = code
				@data1 = data1
				@data2 = data2
			end

			attr_reader :window_id, :code, :data1, :data2
		end

		class Window < Event
			def initialize(type, timestamp, window_id, event, data1, data2)
				super(type, timestamp)
				@window_id = window_id
				@event = event
				@data = [data1, data2].freeze if [:moved, :resized, :size_changed].include?(event)
			end

			attr_reader :window_id, :event, :data
			def data1; data[0] if data end
			def data2; data[1] if data end
		end

		CLASSES = {
			Native::AudioDeviceEvent => AudioDevice,
			Native::ControllerAxisEvent => ControllerAxis,
			Native::ControllerButtonEvent => ControllerButton,
			Native::ControllerDeviceEvent => ControllerDevice,
			Native::DollarGestureEvent => DollarGesture,
			Native::DropEvent => Drop,
			Native::JoyAxisEvent => JoyAxis,
			Native::JoyBallEvent => JoyBall,
			Native::JoyButtonEvent => JoyButton,
			Native::JoyDeviceEvent => JoyDevice,
			Native::JoyHatEvent => JoyHat,
			Native::KeyboardEvent => Keyboard,
			Native::MouseButtonEvent => MouseButton,
			Native::MouseMotionEvent => MouseMotion,
			Native::MouseWheelEvent => MouseWheel,
			Native::MultiGestureEvent => MultiGesture,
			Native::QuitEvent => Quit,
			Native::SysWMEvent => SysWM,
			Native::TextEditingEvent => TextEditing,
			Native::TextInputEvent => TextInput,
			Native::TouchFingerEvent => TouchFinger,
			Native::UserEvent => User,
			Native::WindowEvent => Window
		}.freeze
	end
end
