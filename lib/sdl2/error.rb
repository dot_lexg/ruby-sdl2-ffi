# frozen_string_literal: true

module SDL2
	class Error < ::StandardError; end
end
