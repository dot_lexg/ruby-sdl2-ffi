# frozen_string_literal: true

# The act of requiring 'sdl2/ttf' automatically calls TTF_init().
# require 'sdl2/ttf/native' to prevent this behavior.

require_relative 'ttf/native'
require_relative '../sdl2'

module SDL2
	module TTF
		class Error < ::StandardError; end

		TARGET_VERSION = Native::TARGET_VERSION
		LINKED_VERSION = begin
			version = Native.linked_version
			"#{version[:major]}.#{version[:minor]}.#{version[:patch]}"
		end

		Native.init()

		class Font
			# @param filename [String] Filename of a .ttf or .fon file. FON files
			#   enjoy limited support.
			# @param ptsize [Integer] Point size of font @ 72 DPI. Because of this
			#   choice of DPI, points == pixels.
			# @param index [Integer] Optionally selects a font face from a file
			#   containing multiple font faces. The first face is always index 0. If
			#   the value is too high, the last indexed size will be the default.
			def initialize(filename, ptsize, index = 0)
				@filename = filename
				@ptsize = ptsize
				@index = index
				@native = FFI::AutoPointer.new(
					Native.check(:open_font_index, filename, ptsize, index),
					Native.method(:close_font) # TODO: should probably provide a way to close the font explicitly when you're done with it
				)
				@height = Native.font_height(@native)
				@ascent = Native.font_ascent(@native)
				@descent = Native.font_descent(@native)
				@line_skip = Native.font_line_skip(@native)
				@face_count = Native.font_faces(@native)

				@fixed_width = Native.font_face_is_fixed_width(@native)
				@family_name = Native.font_face_family_name(@native)
				@style_name = Native.font_face_style_name(@native)
			end

			attr_reader :filename, :ptsize, :index, :height, :ascent, :descent, :line_skip, :face_count, :family_name, :style_name

			def fixed_width?
				@fixed_width
			end

			def style
				Native.get_font_style(@native)
			end

			def style=(style)
				Native.set_font_style(@native, style)
			end

			def outline
				outline = Native.get_font_outline(@native)
				outline == 0 ? nil : outline
			end

			def outline=(outline)
				Native.set_font_outline(@native, outline || 0)
			end

			def hinting
				Native.get_font_hinting(@native)
			end

			def hinting=(hinting)
				Native.set_font_hinting(@native, hinting)
			end

			def kerning?
				Native.get_font_kerning(@native)
			end

			def kerning=(kerning)
				Native.set_font_kerning(@native)
			end

			def glyph?(codepoint)
				Native.glyph_is_provided(@native, codepoint)
			end

			Metrics = Struct.new(:minx, :maxx, :miny, :maxy, :advance)
			def glyph_metrics(codepoint)
				minx = Native::IntPtr.new
				maxx = Native::IntPtr.new
				miny = Native::IntPtr.new
				maxy = Native::IntPtr.new
				advance = Native::IntPtr.new
				Native.check(:glyph_metrics, @native, codepoint, minx, maxx, miny, maxy, advance)
				Metrics.new(minx.value, maxx.value, miny.value, maxy.value, advance.value)
			end

			def size(str)
				w = Native::IntPtr.new
				h = Native::IntPtr.new
				Native.check(:size_utf8, @native, str.encode('UTF-8'), w, h)
				[w.value, h.value]
			end

			def render_solid(str, fg:)
				SDL2::Surface.wrap_native_autorelease(Native.check(:render_utf8_solid, @native, str.encode('UTF-8'), SDL2::Native::Color.from_rgba(fg)))
			end

			def render_glyph_solid(codepoint, fg:)
				SDL2::Surface.wrap_native_autorelease(Native.check(:render_glyph_solid, @native, codepoint, SDL2::Native::Color.from_rgba(fg)))
			end

			def render_shaded(str, fg:, bg:)
				SDL2::Surface.wrap_native_autorelease(Native.check(:render_utf8_shaded, @native, str.encode('UTF-8'), SDL2::Native::Color.from_rgba(fg), SDL2::Native::Color.from_rgba(bg)))
			end

			def render_glyph_shaded(codepoint, fg:, bg:)
				SDL2::Surface.wrap_native_autorelease(Native.check(:render_glyph_shaded, @native, codepoint, SDL2::Native::Color.from_rgba(fg), SDL2::Native::Color.from_rgba(bg)))
			end

			# @return [SDL2::Surface]
			def render_blended(str, fg:)
				SDL2::Surface.wrap_native_autorelease(Native.check(:render_utf8_blended, @native, str.encode('UTF-8'), SDL2::Native::Color.from_rgba(fg)))
			end

			def render_blended_wrapped(str, fg:, max_width:)
				SDL2::Surface.wrap_native_autorelease(Native.check(:render_utf8_blended_wrapped, @native, str.encode('UTF-8'), SDL2::Native::Color.from_rgba(fg), max_width))
			end

			def render_glyph_blended(codepoint, fg:)
				SDL2::Surface.wrap_native_autorelease(Native.check(:render_glyph_blended, @native, codepoint, SDL2::Native::Color.from_rgba(fg)))
			end

			def inspect
				<<~INSPECT
					#<#{self.class} #{filename.inspect}[#{index.inspect} of #{face_count.inspect}] #{ptsize.inspect}pt
					family_name=#{family_name.inspect} style_name=#{style_name.inspect} fixed_width?=#{fixed_width?.inspect}
					height=#{height.inspect} ascent=#{ascent.inspect} descent=#{descent.inspect} line_skip=#{line_skip.inspect}
					style=#{style.inspect} outline=#{outline.inspect} hinting=#{hinting.inspect} kerning?=#{kerning?.inspect}>
				INSPECT
			end
		end
	end
end
