# frozen_string_literal: true

# The act of requiring 'sdl2/image' automatically calls IMG_init().
# require 'sdl2/image/native' to prevent this behavior.

require_relative 'image/native'
require_relative '../sdl2'

module SDL2
	module Image
		class Error < ::StandardError; end

		TARGET_VERSION = Native::TARGET_VERSION
		LINKED_VERSION = begin
			version = Native.linked_version
			"#{version[:major]}.#{version[:minor]}.#{version[:patch]}"
		end

		LOADED_IMG_LIBS = Native.init(Native::InitFlags.symbols)

		class << self
			def load(filename)
				Surface.wrap_native(Native.check(:load, filename))
			end

			def save_png(surface, filename)
				Native.check(:save_png, surface.native, filename)
				nil
			end
		end
	end
end
