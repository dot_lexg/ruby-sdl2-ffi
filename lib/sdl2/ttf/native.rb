# frozen_string_literal: true

# The interface and vast majority of the documentation here is adapted from
# https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html and
# https://github.com/libsdl-org/SDL_ttf. See the /reference/ folder for the
# specific header that was used to build this FFI.

require 'ffi'
require_relative '../../sdl2/native'

module SDL2
	module TTF
		# @!group SDL_ttf.h
		# @api native
		module Native
			extend FFI::Library
			ffi_lib 'SDL2_ttf'

			TTF_FONT_CONSTPTR = :pointer
			TTF_FONT_PTR = :pointer
			SDL_RWOPS_PTR = :pointer
			IntPtr = SDL2::Native::IntPtr

			# Calls the given method via .public_send then throws an SDL2::Error when
			# appropriate. This method assumes the function returns a negative integer
			# or NULL on error. See the documentation per function for
			# which functions do this.
			def self.check(method, *args)
				result = public_send(method, *args)

				case result
				when FFI::Struct
					return result if result.pointer != FFI::Pointer::NULL
				when FFI::Pointer
					return result if result != FFI::Pointer::NULL
				when Integer
					return result if result >= 0
				else
					raise "assert: #{method} did not return a pointer or integer"
				end

				error = SDL2::Native.get_error()
				error_class = error == 'That operation is not supported' ? NotImplementedError : SDL2::TTF::Error
				raise error_class, "#{self}.#{method}: #{error}"
			end

			# ZERO WIDTH NO-BREAKSPACE (Unicode byte order mark)
			UNICODE_BOM_NATIVE  = 0xFEFF
			UNICODE_BOM_SWAPPED = 0xFFFE

			Style = bitmask :Style, [
				:bold,          0,
				:italic,        1,
				:underline,     2,
				:strikethrough, 3
			]

			Hinting = enum :Hinting, [
				:normal, 0,
				:light,  1,
				:mono,   2,
				:none,   3
			]

			TARGET_VERSION = '2.0.15'
			attach_function :linked_version, :TTF_Linked_Version, [], SDL2::Native::Version.ptr(:out)

			# XXX: not exposing-- # @!method self.byte_swapped_unicode(swapped)
			# XXX: not exposing-- attach_function :byte_swapped_unicode, :TTF_ByteSwappedUNICODE, [:bool], :void

			# @!method self.init
			# Initialize the truetype font API. This must be called before using other
			# functions in this library, except TTF_WasInit. SDL does not have to be
			# initialized before this call.
			# @return [Integer] 0 if successful, -1 on error (see
			#   {SDL2::Native.get_error} or use {.check})
			attach_function :init, :TTF_Init, [], :int

			# @!method self.open_font(file, ptsize)
			# Load file for use as a font, at ptsize size. This is actually
			# TTF_OpenFontIndex(file, ptsize, 0). This can load TTF and FON files.
			# @param file [String] File name to load font from.
			# @param ptsize [Integer] Point size (based on 72DPI) to load font as.
			#   This basically translates to pixel height.
			# @return [TTF_FONT_PTR]  NULL is returned on errors. (see
			#   {SDL2::Native.get_error} or use {.check})
			attach_function :open_font, :TTF_OpenFont, [:string, :int], TTF_FONT_PTR

			# @!method self.open_font_index(file, ptsize, index)
			#  Load file, face index, for use as a font, at ptsize size. This is
			#  actually TTF_OpenFontIndexRW(SDL_RWFromFile(file), ptsize, index), but
			#  checks that the RWops it creates is not NULL. This can load TTF and FON
			#  files.
			# @param file [String] File name to load font from.
			# @param ptsize [Integer] Point size (based on 72DPI) to load font as.
			#   This basically translates to pixel height.
			# @param index [Integer] choose a font face from a file containing
			#   multiple font faces. The first face is always index 0. If the value is
			#   too high, the last indexed size will be the default.
			# @return [TTF_FONT_PTR]  NULL is returned on errors. (see
			#   {SDL2::Native.get_error} or use {.check})
			attach_function :open_font_index, :TTF_OpenFontIndex, [:string, :int, :long], TTF_FONT_PTR

			# XXX: not exposing-- # @!method self.open_font_rw(src, freesrc, ptsize)
			# XXX: not exposing-- attach_function :open_font_rw, :TTF_OpenFontRW, [SDL_RWOPS_PTR, :int, :int], TTF_FONT_PTR

			# XXX: not exposing-- # @!method self.open_font_index_rw(src, freesrc, ptsize, index)
			# XXX: not exposing-- attach_function :open_font_index_rw, :TTF_OpenFontIndexRW, [SDL_RWOPS_PTR, :int, :int, :long], TTF_FONT_PTR

			# @!method self.get_font_style(font)
			# Get the rendering style of the loaded font.
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the style of.
			# @return [Style]
			attach_function :get_font_style, :TTF_GetFontStyle, [TTF_FONT_CONSTPTR], Style

			# @!method self.set_font_style(font, style)
			# Get the rendering style of the loaded font.
			#
			# NOTE: This will flush the internal cache of previously rendered glyphs,
			# even if there is no change in style, so it may be best to check the
			# current style using TTF_GetFontStyle first.
			#
			# NOTE: TTF_STYLE_UNDERLINE may cause surfaces created by
			# TTF_RenderGlyph_* functions to be extended vertically, downward only, to
			# encompass the underline if the original glyph metrics didn't allow for
			# the underline to be drawn below. This does not change the math used to
			# place a glyph using glyph metrics.
			#
			# On the other hand TTF_STYLE_STRIKETHROUGH doesn't extend the glyph,
			# since this would invalidate the metrics used to position the glyph when
			# blitting, because they would likely be extended vertically upward. There
			# is perhaps a workaround, but it would require programs to be smarter
			# about glyph blitting math than they are currently designed for.
			#
			# Still, sometimes the underline or strikethrough may be outside of the
			# generated surface, and thus not visible when blitted to the screen. In
			# this case, you should probably turn off these styles and draw your own
			# strikethroughs and underlines.
			# @param font [TTF_FONT_PTR] The loaded font to set the style of. It is
			#   the caller's responsibility to nil check this value.
			# @param style [Style]
			# @return [nil]
			attach_function :set_font_style, :TTF_SetFontStyle, [TTF_FONT_PTR, Style], :void

			# @!method self.get_font_outline(font)
			# Get the current outline size of the loaded font.
			# @param font [TTF_FONT_PTR] The loaded font to get the outline size of.
			#   It is the caller's responsibility to nil check this value.
			# @return [Integer] the size of the outline currently set on the font, in
			#   pixels, or 0 if outlining is turned off.
			attach_function :get_font_outline, :TTF_GetFontOutline, [TTF_FONT_CONSTPTR], :int

			# @!method self.set_font_outline(font, outline)
			# Set the outline pixel width of the loaded font.
			#
			# NOTE: This will flush the internal cache of previously rendered glyphs,
			# even if there is no change in outline size, so it may be best to check
			# the current outline size using TTF_GetFontOutline first.
			# @param font [TTF_FONT_PTR] The loaded font to get the outline size of.
			#   It is the caller's responsibility to nil check this value.
			# @param outline [Integer] The size of the outline desired, in pixels, or
			#   0 to turn off outlining.
			# @return [nil]
			attach_function :set_font_outline, :TTF_SetFontOutline, [TTF_FONT_PTR, :int], :void

			# @!method self.get_font_hinting(font)
			# Get the current hinting setting of the loaded font.
			# @param font [TTF_FONT_PTR] The loaded font to get the hinting setting
			#   of. It is the caller's responsibility to nil check this value.
			# @return [Hinting]
			attach_function :get_font_hinting, :TTF_GetFontHinting, [TTF_FONT_CONSTPTR], Hinting

			# @!method self.set_font_hinting(font, hinting)
			# Set the hinting of the loaded font. You should experiment with this
			# setting if you know which font you are using beforehand, especially when
			# using smaller sized fonts. If the user is selecting a font, you may wish
			# to let them select the hinting mode for that font as well.
			#
			# NOTE: This will flush the internal cache of previously rendered glyphs,
			# even if there is no change in hinting, so it may be best to check the
			# current hinting by using TTF_GetFontHinting first.
			# @param font [TTF_FONT_PTR] The loaded font to get the hinting setting
			#   of. It is the caller's responsibility to nil check this value.
			# @param hinting [Hinting]
			# @return [nil]
			attach_function :set_font_hinting, :TTF_SetFontHinting, [TTF_FONT_PTR, Hinting], :void

			# @!method self.font_height(font)
			# Get the maximum pixel height of all glyphs of the loaded font. You may
			# use this height for rendering text as close together vertically as
			# possible, though adding at least one pixel height to it will space it so
			# they can't touch. Remember that SDL_ttf doesn't handle multiline
			# printing, so you are responsible for line spacing, see the
			# TTF_FontLineSkip as well.
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the max height
			#   of. It is the caller's responsibility to nil check this value.
			# @return [Integer] the maximum pixel height of all glyphs in the font.
			attach_function :font_height, :TTF_FontHeight, [TTF_FONT_CONSTPTR], :int

			# @!method self.font_ascent(font)
			# Get the maximum pixel ascent of all glyphs of the loaded font. This can
			# also be interpreted as the distance from the top of the font to the
			# baseline. It could be used when drawing an individual glyph relative to
			# a top point, by combining it with the glyph's maxy metric to resolve the
			# top of the rectangle used when blitting the glyph on the screen.
			#
			# +rect.y = top + TTF_FontAscent(font) - glyph_metric.maxy;+
			#
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the ascent
			#   (height above baseline) of. It is the caller's responsibility to nil
			#   check this value.
			# @return [Integer] the maximum pixel ascent of all glyphs in the font.
			attach_function :font_ascent, :TTF_FontAscent, [TTF_FONT_CONSTPTR], :int

			# Get the offset from the baseline to the bottom of the font
			# This is a negative value, relative to the baseline.

			# @!method self.font_descent(font)
			# Get the maximum pixel descent of all glyphs of the loaded font. This can
			# also be interpreted as the distance from the baseline to the bottom of
			# the font. It could be used when drawing an individual glyph relative to
			# a bottom point, by combining it with the glyph's maxy metric to resolve
			# the top of the rectangle used when blitting the glyph on the screen.
			#
			# +rect.y = bottom - TTF_FontDescent(font) - glyph_metric.maxy;+
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the descent
			#   (height below baseline) of. It is the caller's responsibility to nil
			#   check this value.
			# @return [Integer] the maximum pixel descent of all glyphs in the font.
			attach_function :font_descent, :TTF_FontDescent, [TTF_FONT_CONSTPTR], :int

			# @!method self.font_line_skip(font)
			# Get the recommended pixel height of a rendered line of text of the
			# loaded font. This is usually larger than the TTF_FontHeight of the font.
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the line skip
			#   height of. It is the caller's responsibility to nil check this value.
			# @return [Integer] the recommended pixel height of a rendered line.
			attach_function :font_line_skip, :TTF_FontLineSkip, [TTF_FONT_CONSTPTR], :int

			# @!method self.get_font_kerning(font)
			# Get the current kerning setting of the loaded font. The default for a
			# newly loaded font is enabled (true).
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get kerning setting
			#   of. It is the caller's responsibility to nil check this value.
			# @return [Boolean] whether kerning is enabled.
			attach_function :get_font_kerning, :TTF_GetFontKerning, [TTF_FONT_CONSTPTR], :bool

			# @!method self.set_font_kerning(font, allowed)
			# Set whether to use kerning when rendering the loaded font. This has no
			# effect on individual glyphs, but rather when rendering whole strings of
			# characters, at least a word at a time. Perhaps the only time to disable
			# this is when kerning is not working for a specific font, resulting in
			# overlapping glyphs or abnormal spacing within words. The default for a
			# newly loaded font is enabled (true).
			# @param font [TTF_FONT_PTR] The loaded font to set kerning setting of. It
			#   is the caller's responsibility to nil check this value.
			# @param allowed [Boolean] whether to enable kerning
			# @return [nil]
			attach_function :set_font_kerning, :TTF_SetFontKerning, [TTF_FONT_PTR, :bool], :void

			# @!method self.font_faces(font)
			# Get the number of faces ("sub-fonts") available in the loaded font. This
			# is a count of the number of specific fonts (based on size and style and
			# other typographical features perhaps) contained in the font itself. It
			# seems to be a useless fact to know, since it can't be applied in any
			# other SDL_ttf functions.
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the number of
			#   available faces from. It is the caller's responsibility to nil check
			#   this value.
			# @return [Integer] the number of faces in font.
			attach_function :font_faces, :TTF_FontFaces, [TTF_FONT_CONSTPTR], :long

			# @!method self.font_face_is_fixed_width(font)
			# Test if the current font face of the loaded font is a fixed width font.
			# Fixed width fonts are monospace, meaning every character that exists in
			# the font is the same width, thus you can assume that a rendered string's
			# width is going to be the result of a simple calculation:
			#
			# +glyph_width * string_length+
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the fixed width
			#   status of. It is the caller's responsibility to nil check this value.
			# @return [Boolean] whether font is a fixed width font.
			attach_function :font_face_is_fixed_width, :TTF_FontFaceIsFixedWidth, [TTF_FONT_CONSTPTR], :bool

			# @!method self.font_face_family_name(font)
			# Get the current font face family name from the loaded font. This
			# function may return a NULL pointer, in which case the information is not
			# available.
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the current face
			#   family name of. It is the caller's responsibility to nil check this
			#   value.
			# @return [String] (not const) The current family name of the face of the
			# font, or NULL perhaps.
			attach_function :font_face_family_name, :TTF_FontFaceFamilyName, [TTF_FONT_CONSTPTR], :string

			# @!method self.font_face_style_name(font)
			# Get the current font face style name from the loaded font. This function
			# may return a NULL pointer, in which case the information is not
			# available.
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the current face
			# style name of. It is the caller's responsibility to nil check this
			# value.
			# @return [:string] (not const) The current style name of of the face of
			# the font, or NULL perhaps.
			attach_function :font_face_style_name, :TTF_FontFaceStyleName, [TTF_FONT_CONSTPTR], :string

			# @!method self.glyph_is_provided(font, ch)
			# Get the status of the availability of the glyph for ch from the loaded
			# font.
			# @param font [TTF_FONT_CONSTPTR] The loaded font to get the glyph
			#   availability in. It is the caller's responsibility to nil check this
			#   value.
			# @param ch [Integer] the unicode codepoint to test glyph availability of.
			#   (limited to BMP charset!)
			# @return [Boolean] Whether the glyph is defined. (Note that in C this
			#   also gives us the freetype index, not that it's useful.)
			attach_function :glyph_is_provided, :TTF_GlyphIsProvided, [TTF_FONT_CONSTPTR, :uint16], :bool

			# @!method self.glyph_metrics(font, ch, minx, maxx, miny, maxy, advance)
			# Get desired glyph metrics of the UNICODE chargiven in ch from the loaded
			# font.
			#
			# See https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html#SEC38 for
			# more details.
			#
			# @param font [TTF_FONT_PTR] The loaded font from which to get the glyph
			#   metrics of ch. It is the caller's responsibility to nil check this
			#   value.
			# @param ch [Integer] the UNICODE char to get the glyph metrics for.
			#   (limited to BMP charset!)
			# @param minx [IntPtr.out] the returned minimum X offset into, or NULL
			#   when no return value desired.
			# @param maxx [IntPtr.out] the returned maximum X offset into, or NULL
			#   when no return value desired.
			# @param miny [IntPtr.out] the returned minimum Y offset into, or NULL
			#   when no return value desired.
			# @param maxy [IntPtr.out] the returned maximum Y offset into, or NULL
			#   when no return value desired.
			# @param advance [IntPtr.out] the returned advance offset into, or NULL
			#   when no return value desired.
			# @return [Integer] 0 if successful, -1 on error, such as when the glyph
			#   named by ch does not exist in the font. (see {SDL2::Native.get_error}
			#   or use {.check})
			attach_function :glyph_metrics, :TTF_GlyphMetrics,
				[TTF_FONT_PTR, :uint16, IntPtr.out, IntPtr.out, IntPtr.out, IntPtr.out, IntPtr.out], :int


			# # @!method self.size_text(font, text, w, h)
			# attach_function :size_text, :TTF_SizeText, [TTF_FONT_PTR, :string, IntPtr, IntPtr], :int

			# @!method self.size_utf8(font, text, w, h)
			# Calculate the resulting surface size of the UTF-8 encoded text rendered
			# using font. No actual rendering is done, however correct kerning is done
			# to get the actual width. The height returned in h is the same as you can
			# get using {.font_height}.
			# @param font [TTF_FONT_PTR] The loaded font to use to calculate the size
			# of the string with. It is the caller's responsibility to nil check this
			# value.
			# @param text [String] The UTF-8 string to size up. It is the caller's
			#   responsibility to nil check this value.
			# @param w [IntPtr.out] filled with the text width, or NULL for no desired
			#   return value.
			# @param h [IntPtr.out] filled with the text height, or NULL for no
			#   desired return value.
			# @return [Integer] 0 if successful, -1 on error, such as when the glyph
			#   in the string not being found. (see {SDL2::Native.get_error} or use
			#   {.check})
			attach_function :size_utf8, :TTF_SizeUTF8, [TTF_FONT_PTR, :string, IntPtr.out, IntPtr.out], :int

			# XXX: not exposing-- # @!method self.size_unicode(font, text, w, h)
			# XXX: not exposing-- attach_function :size_unicode, :TTF_SizeUNICODE, [TTF_FONT_PTR, :string, IntPtr, IntPtr], :int

			# XXX: not exposing-- # @!method self.render_text_solid(font, text, fg)
			# XXX: not exposing-- attach_function :render_text_solid, :TTF_RenderText_Solid, [TTF_FONT_PTR, :string, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# @!method self.render_utf8_solid(font, text, fg)
			# Render the UTF-8 encoded text using font with fg color onto a new
			# surface, using the Solid mode.
			# (see https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html#SEC42)
			# @param font [TTF_FONT_PTR] Font to render the text with. It is the
			#   caller's responsibility to nil check this value.
			# @param text [String] The UTF-8 string to render. It is the caller's
			#   responsibility to nil check this value.
			# @param fg [SDL::Native::Color] The color to render the text in. This
			#   becomes colormap index 1.
			# @return [SDL2::Native::Surface] a pointer to a new SDL_Surface. NULL is
			#   returned on errors. (see {SDL2::Native.get_error} or use {.check}).
			#   Caller frees.
			attach_function :render_utf8_solid, :TTF_RenderUTF8_Solid, [TTF_FONT_PTR, :string, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# XXX: not exposing-- # @!method self.render_unicode_solid(font, text, fg)
			# XXX: not exposing-- attach_function :render_unicode_solid, :TTF_RenderUNICODE_Solid, [TTF_FONT_PTR, :string, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# @!method self.render_glyph_solid(font, ch, fg)
			# Render the UTF-8 encoded text using font with fg color onto a new
			# surface, using the Solid mode.
			# (see https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html#SEC42)
			# @param font [TTF_FONT_PTR] Font to render the text with. It is the
			#   caller's responsibility to nil check this value.
			# @param ch [Integer] The unicode codepoint to render. (limited to BMP!)
			# @param fg [SDL::Native::Color] The color to render the text in. This
			#   becomes colormap index 1.
			# @return [SDL2::Native::Surface] a pointer to a new SDL_Surface. NULL is
			#   returned on errors. (see {SDL2::Native.get_error} or use {.check}).
			#   Caller frees.
			attach_function :render_glyph_solid, :TTF_RenderGlyph_Solid, [TTF_FONT_PTR, :uint16, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# XXX: not exposing-- # @!method self.render_text_shaded(font, text, fg, bg)
			# XXX: not exposing-- attach_function :render_text_shaded, :TTF_RenderText_Shaded, [TTF_FONT_PTR, :string, SDL2::Native::Color.val, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# @!method self.render_utf8_shaded(font, text, fg, bg)
			# Render the UTF-8 encoded text using font with fg color onto a new
			# surface, using the Shaded mode.
			# (see https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html#SEC42)
			# @param font [TTF_FONT_PTR] Font to render the text with. It is the
			#   caller's responsibility to nil check this value.
			# @param text [String] The UTF-8 string to render. It is the caller's
			#   responsibility to nil check this value.
			# @param fg [SDL::Native::Color] The color to render the text in. This
			#   becomes colormap index 1.
			# @param bg [SDL::Native::Color] The color to render the background box
			# in. This becomes colormap index 0.
			# @return [SDL2::Native::Surface] a pointer to a new SDL_Surface. NULL is
			#   returned on errors. (see {SDL2::Native.get_error} or use {.check}).
			#   Caller frees.
			attach_function :render_utf8_shaded, :TTF_RenderUTF8_Shaded, [TTF_FONT_PTR, :string, SDL2::Native::Color.val, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# XXX: not exposing-- # @!method self.render_unicode_shaded(font, text, fg, bg)
			# XXX: not exposing-- attach_function :render_unicode_shaded, :TTF_RenderUNICODE_Shaded, [TTF_FONT_PTR, :string, SDL2::Native::Color.val, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# @!method self.render_glyph_shaded(font, ch, fg, bg)
			# Render the UTF-8 encoded text using font with fg color onto a new
			# surface, using the Shaded mode.
			# (see https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html#SEC42)
			# @param font [TTF_FONT_PTR] Font to render the text with. It is the
			#   caller's responsibility to nil check this value.
			# @param ch [Integer] The unicode codepoint to render. (limited to BMP!)
			# @param fg [SDL::Native::Color] The color to render the text in. This
			#   becomes colormap index 1.
			# @param bg [SDL::Native::Color] The color to render the background box
			#   in. This becomes colormap index 0.
			# @return [SDL2::Native::Surface] a pointer to a new SDL_Surface. NULL is
			#   returned on errors. (see {SDL2::Native.get_error} or use {.check}).
			#   Caller frees.
			attach_function :render_glyph_shaded, :TTF_RenderGlyph_Shaded, [TTF_FONT_PTR, :uint16, SDL2::Native::Color.val, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# XXX: not exposing-- # @!method self.render_text_blended(font, text, fg)
			# XXX: not exposing-- attach_function :render_text_blended, :TTF_RenderText_Blended, [TTF_FONT_PTR, :string, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# @!method self.render_utf8_blended(font, text, fg)
			# Render the UTF-8 encoded text using font with fg color onto a new
			# surface, using the Blended mode.
			# (see https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html#SEC42)
			# @param font [TTF_FONT_PTR] Font to render the text with. It is the
			#   caller's responsibility to nil check this value.
			# @param text [String] The UTF-8 string to render. It is the caller's
			#   responsibility to nil check this value.
			# @param fg [SDL::Native::Color] The color to render the text in. Pixels
			#   are blended between transparent and this color to draw the antialiased
			#   glyphs.
			# @return [SDL2::Native::Surface] a pointer to a new SDL_Surface. NULL is
			#   returned on errors. (see {SDL2::Native.get_error} or use {.check}).
			#   Caller frees.
			attach_function :render_utf8_blended, :TTF_RenderUTF8_Blended, [TTF_FONT_PTR, :string, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# XXX: not exposing-- # @!method self.render_unicode_blended(font, text, fg)
			# XXX: not exposing-- attach_function :render_unicode_blended, :TTF_RenderUNICODE_Blended, [TTF_FONT_PTR, :string, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# XXX: not exposing-- # @!method self.render_text_blended_wrapped(font, text, fg, wrapLength)
			# XXX: not exposing-- attach_function :render_text_blended_wrapped, :TTF_RenderText_Blended_Wrapped, [TTF_FONT_PTR, :string, SDL2::Native::Color.val, :uint32], SDL2::Native::Surface.ptr(:out)

			# @!method self.render_utf8_blended_wrapped(font, text, fg, wrapLength)
			# Render the UTF-8 encoded text using font with fg color onto a new
			# surface, using the Blended mode. Not documented in HTML docs.
			# (see https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html#SEC42)
			# @param font [TTF_FONT_PTR] Font to render the text with. It is the
			#   caller's responsibility to nil check this value.
			# @param text [String] The UTF-8 string to render. It is the caller's
			#   responsibility to nil check this value.
			# @param fg [SDL::Native::Color] The color to render the text in. Pixels
			#   are blended between transparent and this color to draw the antialiased
			#   glyphs.
			# @param wrapLength [Integer] Text is wrapped to multiple lines on line
			#   endings and on word boundaries if it extends beyond this length in
			#   pixels.
			# @return [SDL2::Native::Surface] a pointer to a new SDL_Surface. NULL is
			#   returned on errors. (see {SDL2::Native.get_error} or use {.check}).
			#   Caller frees.
			attach_function :render_utf8_blended_wrapped, :TTF_RenderUTF8_Blended_Wrapped, [TTF_FONT_PTR, :string, SDL2::Native::Color.val, :uint32], SDL2::Native::Surface.ptr(:out)

			# XXX: not exposing-- # @!method self.render_unicode_blended_wrapped(font, text, fg, wrapLength)
			# XXX: not exposing-- attach_function :render_unicode_blended_wrapped, :TTF_RenderUNICODE_Blended_Wrapped, [TTF_FONT_PTR, :string, SDL2::Native::Color.val, :uint32], SDL2::Native::Surface.ptr(:out)

			# @!method self.renderglyph_blended(font, ch, fg)
			# Render the UTF-8 encoded text using font with fg color onto a new
			# surface, using the Blended mode.
			# (see https://www.libsdl.org/projects/SDL_ttf/docs/SDL_ttf.html#SEC42)
			# @param font [TTF_FONT_PTR] Font to render the text with. It is the
			#   caller's responsibility to nil check this value.
			# @param ch [Integer] The unicode codepoint to render. (limited to BMP!)
			# @param fg [SDL::Native::Color] The color to render the text in. Pixels
			#   are blended between transparent and this color to draw the antialiased
			#   glyphs.
			# @return [SDL2::Native::Surface] a pointer to a new SDL_Surface. NULL is
			#   returned on errors. (see {SDL2::Native.get_error} or use {.check}).
			#   Caller frees.
			attach_function :render_glyph_blended, :TTF_RenderGlyph_Blended, [TTF_FONT_PTR, :uint16, SDL2::Native::Color.val], SDL2::Native::Surface.ptr(:out)

			# @!method self.close_font(font)
			# Free the memory used by font, and free font itself as well. Do not use
			# font after this without loading a new font to it.
			# @param font [TTF_FONT_PTR] the TTF_Font to free.
			# @return [nil]
			attach_function :close_font, :TTF_CloseFont, [TTF_FONT_PTR], :void

			# @!method self.quit
			# Shutdown and cleanup the truetype font API. After calling this the
			# SDL_ttf functions should not be used, excepting TTF_WasInit. You may, of
			# course, use TTF_Init to use the functionality again.
			# @return [nil]
			attach_function :quit, :TTF_Quit, [], :void

			# @!method self.was_init
			# Query the initialization status of the truetype font API. You may, of
			# course, use this before TTF_Init to avoid initializing twice in a row.
			# Or use this to determine if you need to call TTF_Quit.
			# @return [Boolean] whether TTF has already been initialized.
			attach_function :was_init, :TTF_WasInit, [], :bool

			# @!method self.get_font_kerning_size_glyphs(font, previous_ch, ch)
			# Get the kerning size of two glyphs. Not documented in HTML docs.
			# @param font [TTF_FONT_PTR] The loaded font to get kerning information
			# from. It presumably is the caller's responsibility to nil check this
			# value.
			# @param ch [Integer] the first unicode codepoint to query. (limited to BMP!)
			# @param ch [Integer] the second unicode codepoint to query. (limited to BMP!)
			# @return [Integer] not specifed how to interpret this value ¯\_(ツ)_/¯
			attach_function :get_font_kerning_size_glyphs, :TTF_GetFontKerningSizeGlyphs, [TTF_FONT_PTR, :uint16, :uint16], :int
		end
	end
end
