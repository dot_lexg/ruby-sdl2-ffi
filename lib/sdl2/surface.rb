# frozen_string_literal: true

require_relative 'native'

module SDL2
	class Surface
		def self.wrap_native(native)
			surface = allocate
			surface.initialize_wrapped(native)
			surface
		end

		def self.wrap_native_autorelease(native)
			surface = allocate
			surface.initialize_wrapped(SDL2::Native::Surface.new(FFI::AutoPointer.new(
				native.pointer,
				SDL2::Native.method(:free_surface_ptr)
			)))
			surface
		end

		def initialize_wrapped(native)
			@native = native
		end

		attr_reader :native

		def to_texture(renderer)
			SDL2::Texture.from_surface(renderer, self)
		end

		#TODO: provide this as a monkeypatch
		def save_png(filename)
			SDL2::Image.save_png(self, filename)
		end
	end
end
