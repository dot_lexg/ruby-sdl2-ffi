# frozen_string_literal: true

# The interface and vast majority of the documentation here is adapted from
# https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html and
# https://github.com/libsdl-org/SDL_image. See the /reference/ folder for the
# specific header that was used to build this FFI.

require 'ffi'
require_relative '../../sdl2/native'

module SDL2
	module Image
		# @!group SDL_image.h
		# @api native
		module Native
			extend FFI::Library
			ffi_lib 'SDL2_image'

			TEXTURE_PTR = :pointer
			RENDERER_PTR = :pointer
			RWOPS_PTR = :pointer

			# Calls the given method via #public_send then throws an SDL2::Error when
			# appropriate. This method assumes the function returns a negative integer
			# or NULL on error. See the documentation per function for
			# which functions do this.
			def self.check(method, *args)
				result = public_send(method, *args)

				case result
				when FFI::Struct
					return result if result.pointer != FFI::Pointer::NULL
				when FFI::Pointer
					return result if result != FFI::Pointer::NULL
				when Integer
					return result if result >= 0
				else
					raise "assert: #{method} did not return a pointer or integer"
				end

				error = SDL2::Native.get_error()
				error_class = error == 'That operation is not supported' ? NotImplementedError : SDL2::Image::Error
				raise error_class, "#{self}.#{method}: #{error}"
			end

			TARGET_VERSION = '2.0.5'
			attach_function :linked_version, :IMG_Linked_Version, [], SDL2::Native::Version.ptr(:out)

			InitFlags = bitmask :InitFlags, [
				:jpg,  0,
				:png,  1,
				:tif,  2,
				:webp, 3
			]

			# @!method self.init(flags)
			# Loads dynamic libraries and prepares them for use.
			# @param flags [InitFlags]
			# @return [InitFlags] successfully initialized, or empty array on failure.
			attach_function :init, :IMG_Init, [InitFlags], InitFlags

			# @!method self.quit
			# Unloads libraries loaded with {.init}
			attach_function :quit, :IMG_Quit, [], :void

			# @!method self.load_typed_rw(src, freesrc, type)
			attach_function :load_typed_rw, :IMG_LoadTyped_RW, [RWOPS_PTR, :bool, :string], SDL2::Native::Surface.ptr(:out)
			# @!method self.load(file)
			attach_function :load, :IMG_Load, [:string], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_rw(src, freesrc)
			attach_function :load_rw, :IMG_Load_RW, [RWOPS_PTR, :bool], SDL2::Native::Surface.ptr(:out)

			# @!method self.load_texture(renderer, file)
			attach_function :load_texture, :IMG_LoadTexture, [RENDERER_PTR, :string], TEXTURE_PTR
			# @!method self.load_texture_rw(renderer, src, freesrc)
			attach_function :load_texture_rw, :IMG_LoadTexture_RW, [RENDERER_PTR, RWOPS_PTR, :bool], TEXTURE_PTR
			# @!method self.load_texture_typed_rw(renderer, src, freesrc, type)
			attach_function :load_texture_typed_rw, :IMG_LoadTextureTyped_RW, [RENDERER_PTR, RWOPS_PTR, :bool, :string], TEXTURE_PTR

			# @!method self.is_ico(src)
			attach_function :is_ico, :IMG_isICO, [RWOPS_PTR], :bool
			# @!method self.is_cur(src)
			attach_function :is_cur, :IMG_isCUR, [RWOPS_PTR], :bool
			# @!method self.is_bmp(src)
			attach_function :is_bmp, :IMG_isBMP, [RWOPS_PTR], :bool
			# @!method self.is_gif(src)
			attach_function :is_gif, :IMG_isGIF, [RWOPS_PTR], :bool
			# @!method self.is_jpg(src)
			attach_function :is_jpg, :IMG_isJPG, [RWOPS_PTR], :bool
			# @!method self.is_lbm(src)
			attach_function :is_lbm, :IMG_isLBM, [RWOPS_PTR], :bool
			# @!method self.is_pcx(src)
			attach_function :is_pcx, :IMG_isPCX, [RWOPS_PTR], :bool
			# @!method self.is_png(src)
			attach_function :is_png, :IMG_isPNG, [RWOPS_PTR], :bool
			# @!method self.is_pnm(src)
			attach_function :is_pnm, :IMG_isPNM, [RWOPS_PTR], :bool
			# @!method self.is_svg(src)
			attach_function :is_svg, :IMG_isSVG, [RWOPS_PTR], :bool
			# @!method self.is_tif(src)
			attach_function :is_tif, :IMG_isTIF, [RWOPS_PTR], :bool
			# @!method self.is_xcf(src)
			attach_function :is_xcf, :IMG_isXCF, [RWOPS_PTR], :bool
			# @!method self.is_xpm(src)
			attach_function :is_xpm, :IMG_isXPM, [RWOPS_PTR], :bool
			# @!method self.is_xv(src)
			attach_function :is_xv, :IMG_isXV, [RWOPS_PTR], :bool
			# @!method self.is_webp(src)
			attach_function :is_webp, :IMG_isWEBP, [RWOPS_PTR], :bool

			# @!method self.load_ico_rw(src)
			attach_function :load_ico_rw, :IMG_LoadICO_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_cur_rw(src)
			attach_function :load_cur_rw, :IMG_LoadCUR_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_bmp_rw(src)
			attach_function :load_bmp_rw, :IMG_LoadBMP_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_gif_rw(src)
			attach_function :load_gif_rw, :IMG_LoadGIF_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_jpg_rw(src)
			attach_function :load_jpg_rw, :IMG_LoadJPG_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_lbm_rw(src)
			attach_function :load_lbm_rw, :IMG_LoadLBM_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_pcx_rw(src)
			attach_function :load_pcx_rw, :IMG_LoadPCX_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_png_rw(src)
			attach_function :load_png_rw, :IMG_LoadPNG_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_pnm_rw(src)
			attach_function :load_pnm_rw, :IMG_LoadPNM_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_svg_rw(src)
			attach_function :load_svg_rw, :IMG_LoadSVG_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_tif_rw(src)
			attach_function :load_tif_rw, :IMG_LoadTIF_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_xcf_rw(src)
			attach_function :load_xcf_rw, :IMG_LoadXCF_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_xpm_rw(src)
			attach_function :load_xpm_rw, :IMG_LoadXPM_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_xv_rw(src)
			attach_function :load_xv_rw, :IMG_LoadXV_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)
			# @!method self.load_webp_rw(src)
			attach_function :load_webp_rw, :IMG_LoadWEBP_RW, [RWOPS_PTR], SDL2::Native::Surface.ptr(:out)

			# @!method self.save_png(surface, file)
			attach_function :save_png, :IMG_SavePNG, [SDL2::Native::Surface.ptr(:in), :string], :int
			# @!method self.save_png_rw(surface, dst, freedst)
			attach_function :save_png_rw, :IMG_SavePNG_RW, [SDL2::Native::Surface.ptr(:in), RWOPS_PTR, :bool], :int
			# @!method self.save_jpg(surface, file, quality)
			attach_function :save_jpg, :IMG_SaveJPG, [SDL2::Native::Surface.ptr(:in), :string, :int], :int
			# @!method self.save_jpg_rw(surface, dst, freedst, quality)
			attach_function :save_jpg_rw, :IMG_SaveJPG_RW, [SDL2::Native::Surface.ptr(:in), RWOPS_PTR, :bool, :int], :int
		end
	end
end
