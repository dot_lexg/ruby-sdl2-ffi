# frozen_string_literal: true

require_relative 'native'

module SDL2
	class Renderer
		def initialize(window, *flags, index: -1)
			Native.require_value_type(window, Window)

			@native = FFI::AutoPointer.new(
				Native.check(:create_renderer, window.native, index, Native::RendererFlags[flags]), # TODO: this does not error check
				Native.method(:destroy_renderer)
			)

			info = Native::RendererInfo.new
			Native.check(:get_renderer_info, @native, info)
			@renderer = info[:name]
			@flags = info[:flags]
			@texture_formats = info[:texture_formats].to_a[0...info[:num_texture_formats]]
			@max_texture_size = [info[:max_texture_width], info[:max_texture_height]]
		end

		attr_reader :native

		def new_texture(format, access, width, height)
			Texture.new(self, format, access, width, height)
		end

		def new_static_texture(format, width, height, pixels = nil, pitch = nil)
			texture = Texture.new(self, format, :static, width, height)
			texture.update(pixels, pitch) if pixels
			texture
		end

		def clear
			Native.check(:render_clear, @native)
			nil
		end

		def copy(texture, src: nil, dst: nil)
			Native.require_value_type(texture, Texture)

			src = Rect.new(*src) if src.is_a?(Array) && src.length == 4
			raise ArgumentError, "#{src.inspect} cannot be converted to Rect" if src && !src.is_a?(Rect)

			dst = dst.to_a if dst.respond_to(:to_a)
			if dst.is_a?(Array)
				case dst.length
				when 2
					dst = Rect.new(*dst, src ? src.w : texture.width, src ? src.h : texture.height)
				when 4
					dst = Rect.new(*dst)
				else
					raise ArgumentError, 'Invalid dst argument, expecting [x, y] or [x, y, w, h]'
				end
			end
			raise ArgumentError, "#{dst.inspect} cannot be converted to Rect" if dst && !dst.is_a?(Rect)

			Native.check(:render_copy, @native, texture.native, src&.native, dst&.native)
			nil
		end

		def draw_color=(rgba)
			@draw_color = rgba
			Native.check(:set_render_draw_color, @native, *Color.from_rgba(rgba).to_a)
		end

		def fill_rect(*args)
			rect =
				case args.length
				when 1
					Native::Rect(args.first)
				when 4
					Native::Rect(args)
				else
					raise ArgumentError, "Expected 1 or 4 arguments, got #{args.length}"
				end

			Native.check(:render_fill_rect, @native, rect)
			nil
		end

		def present
			Native.render_present(@native)
		end
	end
end
