# frozen_string_literal: true

require 'ffi'
require_relative 'error'

# @api native
module FFI
	class Struct
		def to_h
			self.members.zip(self.values).to_h
		end
	end

	class Enum
		orig_from_native = instance_method(:from_native)
		define_method(:from_native) do |val, ctx = nil|
			orig_from_native.bind(self).(val, ctx)
		end

		orig_to_native = instance_method(:to_native)
		define_method(:to_native) do |val, ctx = nil|
			orig_to_native.bind(self).(val, ctx)
		end
	end
end

module SDL2
	# @api native
	module Native
		extend FFI::Library
		ffi_lib 'SDL2'

		# Calls the given method via #public_send then throws an SDL2::Error when
		# appropriate. This method assumes the function returns a negative integer,
		# NULL, false, or :unknown on error. See the documentation per function for
		# which functions do this.
		def self.check(method, *args)
			result = public_send(method, *args)

			case result
			when FFI::Struct
				return result if result.pointer != FFI::Pointer::NULL
			when FFI::Pointer
				return result if result != FFI::Pointer::NULL
			when Integer
				return result if result >= 0
			when Symbol
				return result if result != :unknown
			when true, false
				return result if result
			else
				raise "assert: #{method} did not return a pointer, integer, symbol, or boolean"
			end

			error = get_error()
			error_class = error == 'That operation is not supported' ? NotImplementedError : SDL2::Error
			raise error_class, "#{self}.#{method}: #{error}"
		end

		def self.require_value_type(value, type)
			raise ArgumentError, "Object of type #{value.class} cannot be converted to #{type}" if !value.is_a?(type)
		end

		class PrimitivePtr < FFI::Struct
			def value
				self[:value]
			end

			def value=(value)
				self[:value] = value
			end

			def out
				ptr(:out)
			end

			def in
				ptr(:in)
			end

			class << self
				private

				def type(type)
					layout(value: type)
				end
			end
		end
		class FloatPtr < PrimitivePtr; type(:float); end
		class IntPtr < PrimitivePtr; type(:int); end
	end
end

# The FFI bindings required below are structed to match the layout of
# https://wiki.libsdl.org/APIByCategory

# Basics
require_relative 'native/init' # CategoryInit (SDL.h)
# require_relative 'native/hints' # CategoryHints (SDL_hints.h)
require_relative 'native/error' # CategoryError
# require_relative 'native/log' # CategoryLog (SDL_log.h)
# require_relative 'native/assertions' # CategoryAssertions (SDL_assert.h)
require_relative 'native/version' # CategoryVersion (SDL_version.h)

# Video
require_relative 'native/video' # CategoryVideo (SDL_video.h)
require_relative 'native/render' # CategoryRender (SDL_render.h)
require_relative 'native/pixels' # CategoryPixels (SDL_pixels.h)
require_relative 'native/rect' # CategoryRect (SDL_rect.h)
require_relative 'native/surface' # CategorySurface (SDL_surface.h)
# require_relative 'native/swm' # CategorySWM (SDL_syswm.h)
# require_relative 'native/clipboard' # CategoryClipboard (SDL_clipboard.h)
# require_relative 'native/vulkan' # CategoryVulkan (SDL_vulkan.h)

# Input Events
require_relative 'native/events' # CategoryEvents (SDL_events.h)
# require_relative 'native/keyboard' # CategoryKeyboard (SDL_keyboard.h, SDL_keycode.h, SDL_scancode.h)
# require_relative 'native/mouse' # CategoryMouse (SDL_mouse.h)
# require_relative 'native/joystick' # CategoryJoystick (SDL_joystick.h)
# require_relative 'native/game_controller' # CategoryGameController (SDL_gamecontroller.h)
# require_relative 'native/sensor' # CategorySensor (SDL_sensor.h)

# Force Feedback
# require_relative 'native/force_feedback' #  CategoryForceFeedback (SDL_haptic.h)

# Audio
# require_relative 'native/audio' # CategoryAudio (SDL_audio.h)

# Threads
# require_relative 'native/thread' # CategoryThread (SDL_thread.h)
# require_relative 'native/mutex' # CategoryMutex (SDL_mutex.h)
# require_relative 'native/atomic' # CategoryAtomic (SDL_atomic.h)

# Timers
# require_relative 'native/timer' # CategoryTimer (SDL_timer.h)

# File Abstraction
# require_relative 'native/filesystem' # CategoryFilesystem (SDL_filesystem.h)
# require_relative 'native/io' # CategoryIO (SDL_rwops.h)

# Shared Object Support (Will not implement, ffi gem covers this better)

# Platform and CPU Information
# require_relative 'native/platform' # CategoryPlatform (SDL_platform.h)
# require_relative 'native/cpu' # CategoryCPU (SDL_cpuinfo.h)
# require_relative 'native/endian' # CategoryEndian (SDL_endian.h)
# require_relative 'native/bits' # CategoryBits (SDL_bits.h)

# Power Management
# require_relative 'native/power' # CategoryPower (SDL_power.h)

# Plaform-specific Functionality
# require_relative 'native/system' # CategorySystem (SDL_system.h)
# require_relative 'native/standard' # CategoryStandard (SDL_stdinc.h)
