# frozen_string_literal: true

module SDL2
	# @!group SDL_error.h
	# @api native
	module Native
		# @!method self.clear_error
		# @return [nil]
		attach_function :clear_error, :SDL_ClearError, [], :void

		# @!method self.get_error
		# @return [String]
		attach_function :get_error, :SDL_GetError, [], :string

		# @!method self.set_error
		# @return [-1]
		attach_function :set_error, :SDL_SetError, [:string, :varargs], :int
	end
end
