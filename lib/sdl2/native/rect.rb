# frozen_string_literal: true

module SDL2
	# @!group SDL_rect.h
	# @api native
	module Native
		class Point < FFI::Struct
			layout(
				x: :int,
				y: :int
			)
		end

		class Rect < FFI::Struct
			layout(
				x: :int,
				y: :int,
				w: :int,
				h: :int
			)

			#XXX: removing this is a breaking change
			# def initialize(x, y, w, h)
			# 	super()
			# 	self[:x] = x
			# 	self[:y] = y
			# 	self[:w] = w
			# 	self[:h] = h
			# end
		end

		def self.Rect(rect)
			x, y, w, h = rect.is_a?(Array) ? rect : [rect.x, rect.y, rect.w, rect.h]
			rect = Rect.new
			rect[:x] = x
			rect[:y] = y
			rect[:w] = w
			rect[:h] = h
			rect
		end

		# SDL_EnclosePoints
		# SDL_HasIntersection
		# SDL_IntersectRect
		# SDL_IntersectRectAndLine
		# SDL_PointInRect
		# SDL_RectEmpty
		# SDL_RectEquals
		# SDL_UnionRect
	end
end
