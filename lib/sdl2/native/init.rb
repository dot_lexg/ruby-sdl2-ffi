# frozen_string_literal: true

module SDL2
	# @!group SDL_init.h
	# @api native
	module Native
		InitFlags = bitmask FFI::TYPE_UINT32, :InitFlags, [
			:timer, 0,
			:audio, 4,
			:video, 5,
			:joystick, 9,
			:haptic, 12,
			:gamecontroller, 13,
			:events, 14,
			:sensor, 15,
		]

		# @!method self.init(flags)
		# @param flags [InitFlags]
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :init, :SDL_Init, [InitFlags], :int

		# @!method self.init_sub_system(flags)
		# @param flags [InitFlags]
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :init_sub_system, :SDL_InitSubSystem, [InitFlags], :int

		# @!method self.quit
		# @return [nil]
		attach_function :quit, :SDL_Quit, [], :void

		# @!method self.quit_sub_system(flags)
		# @param flags [InitFlags]
		# @return [nil]
		attach_function :quit_sub_system, :SDL_QuitSubSystem, [InitFlags], :void

		# @!method self.set_main_ready(flags)
		# @return [nil]
		attach_function :set_main_ready, :SDL_SetMainReady, [], :void

		# @!method self.was_init(flags)
		# @param flags [InitFlags]
		# @return [InitFlags]
		attach_function :was_init, :SDL_WasInit, [InitFlags], InitFlags

		# NOTE: Not exposing SDL_WinRTRunApp because its unlikely to be useful and upstream docs are incomplete
	end
end
