# frozen_string_literal: true

# The interface and vast majority of the documentation here is adapted from
# https://wiki.libsdl.org/ and https://github.com/libsdl-org/sdl.
# See the /reference/ folder for specific position in history that was used to
# build this FFI.


module SDL2
	# @!group SDL_version.h
	# @api native
	module Native
		# The version of SDL used as reference to build this FFI. The FFI supports dynamic linking to this or later versions.
		TARGET_VERSION = '2.0.12'

		class Version < FFI::Struct
			layout(
				major: :uint8,
				minor: :uint8,
				patch: :uint8
			)
		end

		attach_function :get_version, :SDL_GetVersion, [Version.ptr(:out)], :void

		attach_function :get_revision, :SDL_GetRevision, [], :string

		attach_function :get_revision_number, :SDL_GetRevisionNumber, [], :int
	end
end
