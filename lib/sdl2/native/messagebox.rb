# frozen_string_literal: true

module SDL2
	# @!group SDL_messagebox.h
	# @api native
	module Native
		# Enumerations:
		# SDL_MessageBoxButtonFlags
		# SDL_MessageBoxColorType
		# SDL_MessageBoxFlags

		# Structures:
		# SDL_MessageBoxButtonData
		# SDL_MessageBoxColor
		# SDL_MessageBoxColorScheme
		# SDL_MessageBoxData


		# @!method self.show_message_box(messageboxdata, buttonid)
		attach_function :show_message_box, :SDL_ShowMessageBox, [MessageBoxData.ptr(:in), IntPtr.out], :int

		# @!method self.show_simple_message_box(flags, title, message, window)
		attach_function :show_simple_message_box, :SDL_ShowSimpleMessageBox, [MessageBoxFlags, :string, :string, :pointer], :window
	end
end
