# frozen_string_literal: true

# The interface and vast majority of the documentation here is adapted from
# https://wiki.libsdl.org/ and https://github.com/libsdl-org/sdl.
# See the /reference/ folder for specific position in history that was used to
# build this FFI.


require_relative 'pixels'
require_relative 'rect'

module SDL2
	# @!group SDL_video.h
	# @api native
	module Native
		# @return [Integer] x/y position placeholder to let SDL choose where to position the window on the given display
		def self.windowpos_undefined_display(display_index)
			0x1FFF0000 | display_index
		end

		# @return [Integer] x/y position placeholder to instruct SDL to center the window on the given display
		def self.windowpos_centered_display(display_index)
			0x2FFF0000 | display_index
		end

		WINDOWPOS_UNDEFINED = windowpos_undefined_display(0)
		WINDOWPOS_CENTERED = windowpos_centered_display(0)

		POINT_PTR = :pointer
		SURFACE_PTR = :pointer
		WINDOW_PTR = :pointer

		# BlendMode = enum :BlendMode, []
		# GLattr = enum :GLattr, []
		# GLcontextFlag = enum :GLcontextFlag, []
		# GLprofile = enum :GLprofile, []


		HitTestResult = enum :HitTestResult, [
			:normal,             # Region is normal. No special properties.
			:draggable,          # Region can drag entire window.
			:resize_topleft,
			:resize_top,
			:resize_topright,
			:resize_right,
			:resize_bottomright,
			:resize_bottom,
			:resize_bottomleft,
			:resize_left
		]

		WindowFlags = bitmask FFI::TYPE_UINT32, :WindowFlags, [
			:fullscreen,          0, # fullscreen window
			:opengl,              1, # window usable with an OpenGL context
			:shown,               2,
			:hidden,              3, # window is not visible
			:borderless,          4, # no window decoration
			:resizable,           5, # window can be resized
			:minimized,           6, # window is minimized
			:maximized,           7, # window is maximized
			:input_grabbed,       8, # window has grabbed input focus
			:input_focus,         9,
			:mouse_focus,        10,
			:foreign,            11,
			:fullscreen_desktop, 12, # fullscreen window at desktop resolution, must also set +:fullscreen+
			:allow_highdpi,      13, # window should be created in high-DPI mode if supported (>= SDL 2.0.1)
			:mouse_capture,      14,
			:always_on_top,      15,
			:skip_taskbar,       16,
			:utility,            17,
			:tooltip,            18,
			:popup_menu,         19,
			:vulkan,             20  # window usable with a Vulkan instance
			# missing in header but in docs: SDL_WINDOW_METAL: window usable with a Metal instance
		]

		# Not actually described in header because the C library reuses WindowFlags
		WindowFullscreenFlag = enum FFI::TYPE_UINT32, :WindowFullscreenFlags, [
			:windowed, 0,
			:fullscreen, (1 << 0),
			:fullscreen_desktop, (1 << 12) | (1 << 0)
		]
		class DisplayMode < FFI::Struct
			layout(
				format: PixelFormatEnum,
				w: :int,
				h: :int,
				refresh_rate: :int,
				driverdata: :pointer
			)
		end

		# @!method self.create_window(title, x, y, w, h, flags)
		# Create a window with the specified position, dimensions, and flags.
		#
		# the flag +:show+ is ignored by this function. The SDL_Window is implicitly
		# shown if +:hidden+ is not set. +:show+ may be queried later using
		# {.get_window_flags}.
		#
		# On Apple's macOS, you must set the NSHighResolutionCapable Info.plist
		# property to YES, otherwise you will not receive a High-DPI OpenGL canvas.
		#
		# If the window is created with the +:allow_highdpi+ flag, its size in pixels
		# may differ from its size in screen coordinates on platforms with high-DPI
		# support (e.g. iOS and macOS). Use {.get_window_size} to query the client
		# area's size in screen coordinates, and SDL_GL_GetDrawableSize() or
		# {.get_renderer_output_size} to query the drawable size in pixels.
		#
		# If the window is set fullscreen, the width and height parameters +w+ and
		# +h+ will not be used. However, invalid size parameters (e.g. too large)
		# may still fail. Window size is actually limited to 16384 x 16384 for all
		# platforms at window creation.
		#
		# If the window is created with any of the +:opengl+ or +:vulkan+ flags, then
		# the corresponding LoadLibrary function (SDL_GL_LoadLibrary or
		# SDL_Vulkan_LoadLibrary) is called and the corresponding UnloadLibrary
		# function is called by {.destroy_window}.
		#
		# If +:vulkan+ is specified and there isn't a working Vulkan driver,
		# SDL_CreateWindow() will fail because SDL_Vulkan_LoadLibrary() will fail.
		#
		# If +:metal+ is specified on an OS that does not support Metal,
		# SDL_CreateWindow() will fail.
		#
		# On non-Apple devices, SDL requires you to either not link to the Vulkan
		# loader or link to a dynamic library version. This limitation may be
		# removed in a future version of SDL.
		#
		# @param title [String] the title of the window, in UTF-8 encoding
		# @param x [Integer] the x position of the window, {WINDOWPOS_CENTERED}, or {WINDOWPOS_UNDEFINED}
		# @param y [Integer] the y position of the window, {WINDOWPOS_CENTERED}, or {WINDOWPOS_UNDEFINED}
		# @param w [Integer] the width of the window, in screen coordinates
		# @param h [Integer] the height of the window, in screen coordinates
		# @param flags [WindowFlags] Array of symbols defined in WindowFlags
		# @return [SDL_Window *, FFI::Pointer::NULL] NULL on failure (see {.get_error} or use {.check})
		attach_function :create_window, :SDL_CreateWindow, [:string, :int, :int, :int, :int, WindowFlags], WINDOW_PTR

		# NOTE: not exposing SDL_CreateWindowFrom because anyone skilled enough to use it properly can expose it themselves

		# @!method self.destroy_window(window)
		# Destroy a window.
		# @param window [SDL_Window *]
		# @return [nil]
		attach_function :destroy_window, :SDL_DestroyWindow, [WINDOW_PTR], :void

		# @!method self.disable_screen_saver
		# Prevent the screen from being blanked by a screen saver. If you disable
		# the screensaver, it is automatically re-enabled when SDL quits.
		# @return [nil]
		attach_function :disable_screen_saver, :SDL_DisableScreenSaver, [], :void

		# @!method self.enable_screen_saver
		# Allow the screen to be blanked by a screen saver.
		# @return [nil]
		attach_function :enable_screen_saver, :SDL_EnableScreenSaver, [], :void

		# @!method self.get_closest_display_mode(display_index, mode, closest)
		# Get the closest match to the requested display mode. The available display
		# modes are scanned and closest is filled in with the closest mode matching
		# the requested mode and returned. The mode format and refresh rate default
		# to the desktop mode if they are set to 0. The modes are scanned with size
		# being first priority, format being second priority, and finally checking
		# the refresh rate. If all the available modes are too small, then NULL is
		# returned.
		# @param display_index [Integer] the index of the display to query
		# @param mode [const DisplayMode *] the desired display mode
		# @param closest [out DisplayMode *] filled in with the closest match of the available display modes
		# @return [DisplayMode *, FFL::Pointer::NULL] the passed in value +closest+,
		#                         NULL on failure (see {.get_error} or use {.check})
		attach_function :get_closest_display_mode, :SDL_GetClosestDisplayMode,
			[:int, DisplayMode.ptr(:in), DisplayMode.ptr(:out)], DisplayMode.ptr(:out)

		# @!method self.get_current_display_mode(display_index, mode)
		# Get information about the current display mode. There's a difference
		# between this function and {.get_desktop_display_mode} when SDL runs
		# fullscreen and has changed the resolution. In that case this function will
		# return the current display mode, and not the previous native display mode.
		# @param display_index [Integer] the index of the display to query
		# @param mode [out DisplayMode *] filled in with the current display mode
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_current_display_mode, :SDL_GetCurrentDisplayMode, [:int, DisplayMode.ptr(:out)], :int

		# @!method self.get_current_video_driver
		# Get the name of the currently initialized video driver.
		# @return [String, nil] the name of the current video driver or nil if no driver has been initialized
		attach_function :get_current_video_driver, :SDL_GetCurrentVideoDriver, [], :string

		# @!method self.get_desktop_display_mode(display_index, mode)
		# Get information about the desktop's display mode. There's a difference
		# between this function and {.get_current_display_mode} when SDL runs
		# fullscreen and has changed the resolution. In that case this function will
		# return the previous native display mode, and not the current display mode.
		# @param display_index [Integer] the index of the display to query
		# @param mode [out DisplayMode *] filled in with the current display mode
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_desktop_display_mode, :SDL_GetDesktopDisplayMode, [:int, DisplayMode.ptr(:out)], :int

		# @!method self.get_display_bounds(display_index, rect)
		# Get the desktop area represented by a display. The primary display
		# (+displayIndex+ zero) is always located at 0,0.
		# @param display_index [Integer] the index of the display to query
		# @param rect [out Rect *] filled in with the display bounds
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_display_bounds, :SDL_GetDisplayBounds, [:int, Rect.ptr(:out)], :int

		# @!method self.get_display_dpi(display_index, ddpi, hdpi, vdpi)
		# Get the dots/pixels-per-inch for a display. A failure of this function
		# usually means that either no DPI information is available or the
		# displayIndex is out of range.
		# @param display_index [Integer] the index of the display to query
		# @param ddpi [out float *, nil] filled in with the diagonal DPI of the display
		# @param hdpi [out float *, nil] filled in with the horizontal DPI of the display
		# @param vdpi [out float *, nil] filled in with the vertical DPI of the display
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_display_dpi, :SDL_GetDisplayDPI, [:int, FloatPtr.out, FloatPtr.out, FloatPtr.out], :int

		# @!method self.get_display_mode(display_index, mode_index, mode)
		# Get information about a specific display mode. The display modes are
		# sorted in this priority:
		#
		# * width -> largest to smallest
		# * height -> largest to smallest
		# * bits per pixel -> more colors to fewer colors
		# * packed pixel layout -> largest to smallest
		# * refresh rate -> highest to lowest
		# @param display_index [Integer] the index of the display to query
		# @param mode_index [Integer] see {.get_num_display_modes}
		# @param mode [out DisplayMode *] filled in with the mode at +modeIndex+
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_display_mode, :SDL_GetDisplayMode, [:int, :int, DisplayMode.ptr(:out)], :int

		# @!method self.get_display_name(display_index)
		# Get the name of a display in UTF-8 encoding.
		# @param display_index [Integer] the index of the display to query
		# @return [String] that needs a .force_encoding('UTF-8') before use
		attach_function :get_display_name, :SDL_GetDisplayName, [:int], :string

		# @!method self.get_display_usable_bounds(display_index, rect)
		# Get the usable desktop area represented by a display. This is the same
		# area as SDL_GetDisplayBounds() reports, but with portions reserved by the
		# system removed. For example, on Apple's macOS, this subtracts the area
		# occupied by the menu bar and dock.
		#
		# Setting a window to be fullscreen generally bypasses these unusable areas,
		# so these are good guidelines for the maximum space available to a
		# non-fullscreen window.
		# This function also returns -1 if the parameter displayIndex is out of range.
		# @param display_index [Integer] the index of the display to query
		# @param rect [out Rect *, nil] filled in with the display bounds
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_display_usable_bounds, :SDL_GetDisplayUsableBounds, [:int, Rect.ptr(:out)], :int

		# @!method self.get_grabbed_window
		# Get the window that currently has an input grab enabled.
		# @return [SDL_Window *, FFI::Pointer::NULL] the window if input is grabbed or NULL otherwise
		attach_function :get_grabbed_window, :SDL_GetGrabbedWindow, [], WINDOW_PTR

		# @!method self.get_num_display_modes(display_index)
		# Get the number of available display modes. The +displayIndex+ needs to be
		# in the range from 0 to {.get_num_video_displays} - 1.
		# @param display_index [Integer] the index of the display to query
		# @return [Integer] number of modes on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_num_display_modes, :SDL_GetNumDisplayModes, [:int], :int

		# @!method self.get_num_video_displays
		# Get the number of available video displays.
		# @return [Integer] number of displays on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_num_video_displays, :SDL_GetNumVideoDisplays, [], :int

		# @!method self.get_num_video_drivers
		# Get the number of video drivers compiled into SDL.
		# @return [Integer] number of drivers on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_num_video_drivers, :SDL_GetNumVideoDrivers, [], :int

		# @!method self.get_video_driver(index)
		# Get the name of a built in video driver. The video drivers are presented
		# in the order in which they are normally checked during initialization.
		# @param index [Integer] the index of a video driver (in range of 0 to {.get_num_video_drivers} - 1)
		# @return [String] the name of the video driver with the given +index+
		attach_function :get_video_driver, :SDL_GetVideoDriver, [:int], :string

		# @!method self.get_window_borders_size(window, top, left, bottom, right)
		# Get the size of a window's borders (decorations) around the client area.
		#
		# Note: If this function fails (returns -1), the size values will be
		# initialized to 0, 0, 0, 0 (if a non-NULL pointer is provided), as if the
		# window in question was borderless.
		#
		#	This function also returns -1 if getting the information is not supported.
		#
		# Note: not supported by cocoa
		# @param window [SDL_Window *]
		# @param top [out int *, nil] pointer to variable for storing the size of the top border
		# @param left [out int *, nil] pointer to variable for storing the size of the left border
		# @param bottom [out int *, nil] pointer to variable for storing the size of the bottom border
		# @param right [out int *, nil] pointer to variable for storing the size of the right border
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_window_borders_size, :SDL_GetWindowBordersSize,
			[WINDOW_PTR, IntPtr.out, IntPtr.out, IntPtr.out, IntPtr.out], :int

		# @!method self.get_window_brightness(window, brightness)
		# Get the brightness (gamma multiplier) for a given window's display.
		# Despite the name and signature, this method retrieves the brightness of
		# the entire display, not an individual window. A window is considered to be
		# owned by the display that contains the window's center pixel. (The index
		# of this display can be retrieved using {.get_window_display_index}.)
		attach_function :get_window_brightness, :SDL_GetWindowBrightness, [WINDOW_PTR], :float

		# @!method self.get_window_data(window, name)
		# Retrieve the data pointer associated with a window.
		#
		# Not exposing to ruby-dialiect API, reimplemented in pure ruby via {SDL2::Window#tag}
		# @param window [SDL_Window *] the window to query
		# @param name [String] the name of the pointer
		# @return [void *] the value associated with +name+
		attach_function :get_window_data, :SDL_GetWindowData, [WINDOW_PTR, :string], :pointer

		# @!method self.get_window_display_index(window)
		# Get the index of the display associated with a window.
		# @param window [SDL_Window *] the window to query
		# @return [Integer] the index of the display containing the center of the window on success,
		#                   < 0 on error (see {.get_error} or use {.check})
		attach_function :get_window_display_index, :SDL_GetWindowDisplayIndex, [WINDOW_PTR], :int

		# @!method self.get_window_display_mode(window, mode)
		# Query the display mode to use when a window is visible at fullscreen.
		# @param window [SDL_Window *] the window to query
		# @param mode [out DisplayMode *] filled in with the fullscreen display mode
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_window_display_mode, :SDL_GetWindowDisplayMode, [WINDOW_PTR, DisplayMode.ptr(:out)], :int

		# @!method self.get_window_flags(window)
		# Get the window flags.
		# @param window [SDL_Window *] the window to query
		# @return [WindowFlags] an array of the {WindowFlags} associated with +window+
		attach_function :get_window_flags, :SDL_GetWindowFlags, [WINDOW_PTR], WindowFlags

		# @!method self.get_window_from_id(id)
		# Get a window from a stored ID.
		# @param id [Integer] the ID of the window
		# @return [SDL_Window *] Returns the window associated with +id+,
		#                        NULL if it doesn't exist (see {.get_error} or use {.check})
		attach_function :get_window_from_id, :SDL_GetWindowFromID, [:uint32], WINDOW_PTR

		# @!method self.get_window_gamma_ramp(window, red, green, blue)
		# Get the gamma ramp for a given window's display. Despite the name and
		# signature, this method retrieves the gamma ramp of the entire display, not
		# an individual window. A window is considered to be owned by the display
		# that contains the window's center pixel. (The index of this display can be
		# retrieved using {.get_window_display_index}.)
		# @param window [SDL_Window *] the window used to select the display whose gamma ramp will be queried
		# @param red [out uint16 *] a 256 element array of 16-bit quantities filled in
		#                           with the translation table for the red channel
		# @param green [out uint16 *] a 256 element array of 16-bit quantities filled in
		#                             with the translation table for the green channel
		# @param blue [out uint16 *] a 256 element array of 16-bit quantities filled in
		#                            with the translation table for the blue channel
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_window_gamma_ramp, :SDL_GetWindowGammaRamp, [WINDOW_PTR, :pointer, :pointer, :pointer], :int

		# @!method self.get_window_grab(window)
		# Get a window's input grab mode.
		# @param window [SDL_Window *] the window to query
		# @return [Boolean] true if input is grabbed, false otherwise
		attach_function :get_window_grab, :SDL_GetWindowGrab, [WINDOW_PTR], :bool

		# @!method self.get_window_id(window)
		# Get the numeric ID of a window. The numeric ID is what SDL_WindowEvent
		# references, and is necessary to map these events to specific SDL_Window
		# objects.
		# @param window [SDL_Window *] the window to query
		# @return [Integer] the ID of the window on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_window_id, :SDL_GetWindowID, [WINDOW_PTR], :uint32

		# @!method self.get_window_maximum_size(window, w, h)
		# Get the maximum size of a window's client area.
		# @param window [SDL_Window *] the window to query
		# @param w [out IntPtr, nil] filled in with the maximum width of the window
		# @param h [out IntPtr, nil] filled in with the maximum height of the window
		# @return [nil]
		attach_function :get_window_maximum_size, :SDL_GetWindowMaximumSize, [WINDOW_PTR, IntPtr.out, IntPtr.out], :void

		# @!method self.get_window_minimum_size(window, w, h)
		# Get the minimum size of a window's client area.
		# @param window [SDL_Window *] the window to query
		# @param w [out IntPtr, nil] filled in with the minimum width of the window
		# @param h [out IntPtr, nil] filled in with the minimum height of the window
		# @return [nil]
		attach_function :get_window_minimum_size, :SDL_GetWindowMinimumSize, [WINDOW_PTR, IntPtr.out, IntPtr.out], :void

		# @!method self.get_window_opacity(window, opacity)
		# Get the opacity of a window. If transparency isn't supported on this
		# platform, opacity will be reported as 1.0f without error.
		# @param window [SDL_Window *] the window to query
		# @param opacity [out FloatPtr, nil] the float filled in (0.0f - transparent, 1.0f - opaque)
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_window_opacity, :SDL_GetWindowOpacity, [WINDOW_PTR, FloatPtr.out], :int

		# @!method self.get_window_pixel_format(window)
		# Get the pixel format associated with the window.
		# @param window [SDL_Window *] the window to query
		# @return [PixelFormatEnum] the pixel format of the window on success,
		#                           :unknown on error (see {.get_error} or use {.check})
		attach_function :get_window_pixel_format, :SDL_GetWindowPixelFormat, [WINDOW_PTR], PixelFormatEnum

		# @!method self.get_window_position(window, x, y)
		# Get the position of a window.
		# @param window [SDL_Window *] the window to query
		# @param x [out IntPtr, nil] filled in with the x position of the window, in screen coordinates
		# @param y [out IntPtr, nil] filled in with the y position of the window, in screen coordinates
		# @return [nil]
		attach_function :get_window_position, :SDL_GetWindowPosition, [WINDOW_PTR, IntPtr.out, IntPtr.out], :void

		# @!method self.get_window_size(window, w, h)
		# Get the size of a window's client area.
		#
		# The window size in screen coordinates may differ from the size in pixels,
		# if the window was created with the :allow_highdpi flag on a platform with
		# high-dpi support (e.g. iOS or macOS). Use SDL_GL_GetDrawableSize(),
		# SDL_Vulkan_GetDrawableSize(), or {.get_renderer_output_size} to get the
		# real client area size in pixels.
		# @param window [SDL_Window *] the window to query
		# @param w [out IntPtr] a pointer filled in with the width of the window, in screen coordinates
		# @param h [out IntPtr] a pointer filled in with the height of the window, in screen coordinates
		# @return [nil]
		attach_function :get_window_size, :SDL_GetWindowSize, [WINDOW_PTR, IntPtr.out, IntPtr.out], :void

		# @!method self.get_window_surface(window)
		# Get the SDL surface associated with the window. A new surface will be
		# created with the optimal format for the window, if necessary. This surface
		# will be freed when the window is destroyed. Do not free this surface.
		#
		# This surface will be invalidated if the window is resized. After resizing
		# a window this function must be called again to return a valid surface.
		#
		# You may not combine this with 3D or the rendering API on this window.
		#
		# This function is affected by SDL_HINT_FRAMEBUFFER_ACCELERATION.
		# @param window [SDL_Window *] the window to query
		# @return [SDL_Surface *, FFI::Pointer::NULL] the surface associated with the window, NULL on error (see {.get_error} or use {.check})
		attach_function :get_window_surface, :SDL_GetWindowSurface, [WINDOW_PTR], SURFACE_PTR

		# @!method self.get_window_title(window)
		# Get the title of a window.
		# @param window [SDL_Window *] the window to query
		# @return [String] the title of the window in UTF-8 format or "" if there is no title
		#                  Must do +.force_encoding('UTF-8')+ on the result
		attach_function :get_window_title, :SDL_GetWindowTitle, [WINDOW_PTR], :string

		# TODO: not ready for primetime, need to look over SysWMInfo headers and docs before exposing this
		# @!method self.get_window_wminfo(window, info)
		# Get driver-specific information about a window.
		#
		# This method depends on interface specified in +SDL_syswm.h+
		#
		# The caller must initialize the info structure's version by using
		# SDL_VERSION(&info.version), and then this function will fill in the rest
		# of the structure with information about the given window.
		# @param window [SDL_Window *] the window to query
		# @param info [out SDL_SysWMinfo *] # TODO: update attach_function type to structure when it's specified in this FFI
		# @return [Boolean] true if implemented and +info.version+ is valid, false otherwise (see {.get_error} or use {.check})
		# attach_function :get_window_wminfo, :SDL_GetWindowWMInfo, [WINDOW_PTR, :pointer], :bool

		# TODO: consider exposing these (clients will need an FFI for OpenGL to use!)
		# attach_function :gl__create_context, :SDL_GL_CreateContext
		# attach_function :gl__delete_context, :SDL_GL_DeleteContext
		# attach_function :gl__extension_supported, :SDL_GL_ExtensionSupported
		# attach_function :gl__get_attribute, :SDL_GL_GetAttribute
		# attach_function :gl__get_current_context, :SDL_GL_GetCurrentContext
		# attach_function :gl__get_current_window, :SDL_GL_GetCurrentWindow
		# attach_function :gl__get_drawable_size, :SDL_GL_GetDrawableSize
		# attach_function :gl__get_proc_address, :SDL_GL_GetProcAddress
		# attach_function :gl__get_swap_interval, :SDL_GL_GetSwapInterval
		# attach_function :gl__load_library, :SDL_GL_LoadLibrary
		# attach_function :gl__make_current, :SDL_GL_MakeCurrent
		# attach_function :gl__reset_attributes, :SDL_GL_ResetAttributes
		# attach_function :gl__set_attribute, :SDL_GL_SetAttribute
		# attach_function :gl__set_swap_interval, :SDL_GL_SetSwapInterval
		# attach_function :gl__swap_window, :SDL_GL_SwapWindow
		# attach_function :gl__unload_library, :SDL_GL_UnloadLibrary

		# @!method self.hide_window(window)
		# Hide a window.
		# @param window [SDL_Window *] the window to hide
		# @return [nil]
		attach_function :hide_window, :SDL_HideWindow, [WINDOW_PTR], :void

		# @!method self.is_screen_saver_enabled
		# Check whether the screensaver is currently enabled.
		#
		# The screensaver is disabled by default since SDL 2.0.2. Before SDL 2.0.2 the screensaver was enabled by default.
		# @return [Boolean] true if enabled, false if disabled
		attach_function :is_screen_saver_enabled, :SDL_IsScreenSaverEnabled, [], :bool

		# @!method self.maximize_window(window)
		# Make a window as large as possible.
		# @param window [SDL_Window *] the window to maximize
		# @return [nil]
		attach_function :maximize_window, :SDL_MaximizeWindow, [WINDOW_PTR], :void

		# @!method self.minimize_window(window)
		# Minimize a window to an iconic representation.
		# @param window [SDL_Window *] the window to minimize
		# @return [nil]
		attach_function :minimize_window, :SDL_MinimizeWindow, [WINDOW_PTR], :void

		# @!method self.raise_window(window)
		# Raise a window above other windows and set the input focus.
		# @param window [SDL_Window *] the window to raise
		# @return [nil]
		attach_function :raise_window, :SDL_RaiseWindow, [WINDOW_PTR], :void

		# @!method self.restore_window(window)
		# Restore the size and position of a minimized or maximized window.
		# @param window [SDL_Window *] the window to restore
		# @return [nil]
		attach_function :restore_window, :SDL_RestoreWindow, [WINDOW_PTR], :void

		# @!method self.set_window_bordered(window, bordered)
		# Set the border state of a window. This will add or remove the window's
		# :borderless flag and add or remove the border from the actual window. This
		# is a no-op if the window's border already matches the requested state.
		#
		# You can't change the border state of a fullscreen window.
		# @param window [SDL_Window *] the window of which to change the border state
		# @param bordered [Boolean] false to remove border, true to add border
		# @return [nil]
		attach_function :set_window_bordered, :SDL_SetWindowBordered, [WINDOW_PTR, :bool], :void

		# @!method self.set_window_brightness(window, brightness)
		# Set the brightness (gamma multiplier) for a given window's display.
		#
		# Despite the name and signature, this method sets the brightness of the
		# entire display, not an individual window. A window is considered to be
		# owned by the display that contains the window's center pixel. (The index
		# of this display can be retrieved using SDL_GetWindowDisplayIndex().) The
		# brightness set will not follow the window if it is moved to another
		# display.
		#
		# Many platforms will refuse to set the display brightness in modern times.
		# You are better off using a shader to adjust gamma during rendering, or
		# something similar.
		# @param window [SDL_Window *] the window used to select the display whose brightness will be changed
		# @param brightness [Float] the brightness value where 0.0 is completely dark and 1.0 is normal brightness
		# @return [nil]
		attach_function :set_window_brightness, :SDL_SetWindowBrightness, [WINDOW_PTR, :float], :void

		# @!method self.set_window_data(window, name, userdata)
		# Associate an arbitrary named pointer with a window.
		#
		# Not exposing to ruby-dialiect API, reimplemented in pure ruby via {SDL2::Window#tag=}
		# @param window [SDL_Window *] the window to associate with the pointer
		# @param name [String] the name of the pointer, case-sensitive
		# @param userdata [void *] the associated pointer
		# @return [void *] the previous value associated with +name+
		attach_function :set_window_data, :SDL_SetWindowData, [WINDOW_PTR, :string, :pointer], :pointer

		# @!method self.set_window_display_mode(window, mode)
		# Set the display mode to use when a window is visible at fullscreen.
		#
		# This only affects the display mode used when the window is fullscreen. To
		# change the window size when the window is not fullscreen, use
		# {.set_window_size}.
		# @param window [SDL_Window *] the window to affect
		# @param mode [DisplayMode *, nil] the mode to use, or
		#                                  nil to use the window's dimensions and the desktop's format and refresh rate
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :set_window_display_mode, :SDL_SetWindowDisplayMode, [WINDOW_PTR, DisplayMode.ptr(:in)], :int

		# @!method self.set_window_fullscreen(window, flags)
		# Set a window's fullscreen state.
		# @param window [SDL_Window *] the window to change
		# @param flags [WindowFullscreenFlag] :fullscreen for "real" fullscreen with a videomode change,
		#                                     :fullscreen_desktop for "fake" fullscreen that takes the size of the desktop,
		#                                     or :windowed to turn fullscreen off
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :set_window_fullscreen, :SDL_SetWindowFullscreen, [WINDOW_PTR, WindowFullscreenFlag], :int

		# @!method self.set_window_gamma_ramp(window, red, green, blue)
		# Set the gamma ramp at the video hardware level for the display that owns a given window.
		# Each array represents a mapping between the input and output for that channel. The
		# input is the index into the array, and the output is the 16-bit gamma
		# value at that index, scaled to the output color precision.
		#
		# Despite the name and signature, this method sets the gamma ramp of the
		# entire display, not an individual window. A window is considered to be
		# owned by the display that contains the window's center pixel. (The index
		# of this display can be retrieved using {.get_window_display_index}.) The
		# gamma ramp set will not follow the window if it is moved to another
		# display.
		# @param window [SDL_Window *] the window used to select the display whose gamma ramp will be changed
		# @param red [uint16 *, nil] a 256 element array representing the translation table for the red channel, or NULL
		# @param green [uint16 *, nil] a 256 element array representing the translation table for the green channel, or NULL
		# @param blue [uint16 *, nil] a 256 element array representing the translation table for the blue channel, or NULL
		# @return [Integer]
		attach_function :set_window_gamma_ramp, :SDL_SetWindowGammaRamp, [WINDOW_PTR, :pointer, :pointer, :pointer], :int

		# @!method self.set_window_grab(window, grabbed)
		# Set a window's input grab mode. When input is grabbed the mouse is
		# confined to the window.
		#
		# If the caller enables a grab while another window is currently grabbed,
		# the other window loses its grab in favor of the caller's window.
		# @param window [SDL_Window *] the window for which the input grab mode should be set
		# @param grabbed [Boolean] true to grab input, false to release input
		# @return [nil]

		attach_function :set_window_grab, :SDL_SetWindowGrab, [WINDOW_PTR, :bool], :void

		callback :hit_test, [WINDOW_PTR, Point.ptr, :pointer], HitTestResult
		# @!method self.set_window_hit_test(window, callback, callback_data)
		# Provide a callback that decides if a window region has special properties.
		# Normally windows are dragged and resized by decorations provided by the
		# system window manager (a title bar, borders, etc), but for some apps, it
		# makes sense to drag them from somewhere else inside the window itself; for
		# example, one might have a borderless window that wants to be draggable
		# from any part, or simulate its own title bar, etc.
		#
		# This function lets the app provide a callback that designates pieces of a
		# given window as special. This callback is run during event processing if
		# we need to tell the OS to treat a region of the window specially; the use
		# of this callback is known as "hit testing."
		#
		# Mouse input may not be delivered to your application if it is within a
		# special area; the OS will often apply that input to moving the window or
		# resizing the window and not deliver it to the application.
		#
		# Platforms that don't support this functionality will return -1
		# unconditionally, even if you're attempting to disable hit-testing.
		#
		# Your callback may fire at any time, and its firing does not indicate any
		# specific behavior, it can fire more often than you'd expect. Since this
		# can fire at any time, you should try to keep your callback efficient,
		# devoid of allocations, etc.
		# @param window [SDL_Window *] the window to set hit-testing on
		# @param callback [proc |window: SDL_Window *, area: SDL_Point *, data: void *| => HitTestResult, nil]
		#                 the function to call when doing a hit-test, nil to disable hit-testing
		# @param callback_data [void *] an app-defined pointer passed to +callback+
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :set_window_hit_test, :SDL_SetWindowHitTest, [WINDOW_PTR, :hit_test, :pointer], :int

		# @!method self.set_window_icon(window, icon)
		# Set the icon for a window.
		# @param window [SDL_Window *] the window to change
		# @param icon [SDL_Surface *] containing the icon for the window
		# @return [nil]
		attach_function :set_window_icon, :SDL_SetWindowIcon, [WINDOW_PTR, SURFACE_PTR], :void

		# @!method self.set_window_input_focus(window)
		# Explicitly set input focus to the window, without raising it. You almost
		# certainly want {.raise_window} instead of this function. Use this with
		# caution, as you might give focus to a window that is completely obscured
		# by other windows.
		# @param window [SDL_Window *] the window that should get the input focus
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :set_window_input_focus, :SDL_SetWindowInputFocus, [WINDOW_PTR], :int

		# @!method self.set_window_maximum_size(window, max_w, max_h)
		# Set the maximum size of a window's client area.
		# @param window [SDL_Window *] the window to change
		# @param max_w [Integer] the maximum width of the window in pixels
		# @param max_h [Integer] the maximum height of the window in pixels
		# @return [nil]
		attach_function :set_window_maximum_size, :SDL_SetWindowMaximumSize, [WINDOW_PTR, :int, :int], :void

		# @!method self.set_window_minimum_size(window, min_w, min_h)
		# Set the minimum size of a window's client area.
		# @param window [SDL_Window *] the window to change
		# @param min_w [Integer] the minimum width of the window in pixels
		# @param min_h [Integer] the minimum height of the window in pixels
		# @return [nil]
		attach_function :set_window_minimum_size, :SDL_SetWindowMinimumSize, [WINDOW_PTR, :int, :int], :void

		# @!method self.set_window_modal_for(modal_window, parent_window)
		# Set the window as a modal for another window.
		# @param modal_window [SDL_Window *] the window that should be set modal
		# @param parent_window [SDL_Window *] the parent window for the modal window
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :set_window_modal_for, :SDL_SetWindowModalFor, [WINDOW_PTR, WINDOW_PTR], :int

		# @!method self.set_window_opacity(window, opacity)
		# Set the opacity for a window.
		#
		# May not be supported on all platforms.
		# @param window [SDL_Window *] the window for which to change opacity
		# @param opacity [Float] the opacity value (0.0f - transparent, 1.0f - opaque).
		#                        Values outside this range will be clamped
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :set_window_opacity, :SDL_SetWindowOpacity, [WINDOW_PTR, :float], :int

		# @!method self.set_window_position(window, x, y)
		# Set the position of a window.
		# @param window [SDL_Window *] the window to reposition
		# @param x [Integer] the x coordinate of the window in screen coordinates,
		#                    {WINDOWPOS_CENTERED}, or {WINDOWPOS_UNDEFINED}
		# @param y [Integer] the y coordinate of the window in screen coordinates,
		#                    {WINDOWPOS_CENTERED}, or {WINDOWPOS_UNDEFINED}
		# @return [nil]
		attach_function :set_window_position, :SDL_SetWindowPosition, [WINDOW_PTR, :int, :int], :void

		# @!method self.set_window_resizable(window, resizable)
		# Set the user-resizable state of a window. This will add or remove the
		# window's +:window_resizable+ flag and allow/disallow user resizing of the
		# window. This is a no-op if the window's resizable state already matches
		# the requested state.
		#
		# You can't change the resizable state of a fullscreen window.
		# @param window [SDL_Window *] the window of which to change the resizable state
		# @param resizable [Boolean] true to allow resizing, false to disallow resizing
		# @return [nil]
		attach_function :set_window_resizable, :SDL_SetWindowResizable, [WINDOW_PTR, :bool], :void

		# @!method self.set_window_size(window, w, h)
		# Set the size of a window's client area.
		#
		# The window size in screen coordinates may differ from the size in pixels,
		# if the window was created with the +:allow_highdpi+ flag on a platform
		# with high-dpi support (e.g. iOS or macOS). Use SDL_GL_GetDrawableSize() or
		# {.get_renderer_output_size} to get the real client area size in pixels.
		#
		# Fullscreen windows automatically match the size of the display mode, and
		# you should use {.set_window_display_mode} to change their size.
		# @param window [SDL_Window *] the window to change
		# @param w [Integer] the width of the window in pixels, in screen coordinates, must be > 0
		# @param h [Integer] the height of the window in pixels, in screen coordinates, must be > 0
		# @return [nil]
		attach_function :set_window_size, :SDL_SetWindowSize, [WINDOW_PTR, :int, :int], :void

		# @!method self.set_window_title(window, title)
		# Set the title of a window.
		# @param window [SDL_Window *] the window to change
		# @param title [String] the desired window title in UTF-8 format
		# @return [nil]
		attach_function :set_window_title, :SDL_SetWindowTitle, [WINDOW_PTR, :string], :void

		# @!method self.show_window(window)
		# Show a window.
		# @param window [SDL_Window *] the window to show
		# @return [nil]
		attach_function :show_window, :SDL_ShowWindow, [WINDOW_PTR], :void

		# @!method self.update_window_surface(window)
		# Copy the window surface to the screen. This is the function you use to
		# reflect any changes to the surface on the screen.
		#
		# This function is equivalent to the SDL 1.2 API SDL_Flip().
		# @param window [SDL_Window *] the window to update
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :update_window_surface, :SDL_UpdateWindowSurface, [WINDOW_PTR], :int

		# @!method self.update_window_surface_rects(window, rects, numrects)
		# Copy areas of the window surface to the screen. This is the function you
		# use to reflect changes to portions of the surface on the screen.
		# TODO: expose to ruby-dialected API
		#
		# This function is equivalent to the SDL 1.2 API SDL_UpdateRects().
		# @param window [SDL_Window *] the window to update
		# @param rects [SDL_Rect *] array with +numrects+ elements representing areas of the surface to copy
		# @param numrects [Integer] the number or elements in +rects+
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :update_window_surface_rects, :SDL_UpdateWindowSurfaceRects, [WINDOW_PTR, :pointer, :int], :int

		# @!method self.video_init(driver_name)
		# Initialize the video subsystem, optionally specifying a video driver. This
		# function initializes the video subsystem, setting up a connection to the
		# window manager, etc, and determines the available display modes and pixel
		# formats, but does not initialize a window or graphics mode.
		# TODO: expose to ruby-dialected API
		#
		# If you use this function and you haven't used the :video flag with either
		# {.init} or {.init_sub_system}, you should call {.video_quit} before
		# calling {.quit}.
		#
		# It is safe to call this function multiple times. this function will call
		# {.video_quit} itself if the video subsystem has already been initialized.
		#
		# You can use {.get_num_video_drivers} and {.getVideoDriver} to find a
		# specific +driver_name+.
		# @param driver_name [String, nil] the name of a video driver to initialize, or nil for the default driver
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :video_init, :SDL_VideoInit, [:string], :int

		# @!method self.video_quit
		# Shut down the video subsystem, if initialized with {.video_init}.
		# TODO: expose to ruby-dialected API
		#
		# This function closes all windows, and restores the original video mode.
		# @return [nil]
		attach_function :video_quit, :SDL_VideoQuit, [], :void
	end
end
