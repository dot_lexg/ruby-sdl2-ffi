# frozen_string_literal: true

require_relative 'scancode'

module SDL2
	# @!group SDL_keycode.h
	# @api native
	module Native
		Keycode = enum FFI::TYPE_INT32, :Keycode, [
			:UNKNOWN, 0,
			:RETURN, '\r'.ord,
			:ESCAPE, '\033'.ord,
			:BACKSPACE, '\b'.ord,
			:TAB, '\t'.ord,
			:SPACE, ' '.ord,
			:EXCLAIM, '!'.ord,
			:QUOTEDBL, '"'.ord,
			:HASH, '#'.ord,
			:PERCENT, '%'.ord,
			:DOLLAR, '$'.ord,
			:AMPERSAND, '&'.ord,
			:QUOTE, '\''.ord,
			:LEFTPAREN, '('.ord,
			:RIGHTPAREN, ')'.ord,
			:ASTERISK, '*'.ord,
			:PLUS, '+'.ord,
			:COMMA, '.ord,'.ord,
			:MINUS, '-'.ord,
			:PERIOD, '.'.ord,
			:SLASH, '/'.ord,
			:K_0, '0'.ord,
			:K_1, '1'.ord,
			:K_2, '2'.ord,
			:K_3, '3'.ord,
			:K_4, '4'.ord,
			:K_5, '5'.ord,
			:K_6, '6'.ord,
			:K_7, '7'.ord,
			:K_8, '8'.ord,
			:K_9, '9'.ord,
			:COLON, ':'.ord,
			:SEMICOLON, ';'.ord,
			:LESS, '<'.ord,
			:EQUALS, '='.ord,
			:GREATER, '>'.ord,
			:QUESTION, '?'.ord,
			:AT, '@'.ord,
			:LEFTBRACKET, '['.ord,
			:BACKSLASH, '\\'.ord,
			:RIGHTBRACKET, ']'.ord,
			:CARET, '^'.ord,
			:UNDERSCORE, '_'.ord,
			:BACKQUOTE, '`'.ord,
			:A, 'a'.ord,
			:B, 'b'.ord,
			:C, 'c'.ord,
			:D, 'd'.ord,
			:E, 'e'.ord,
			:F, 'f'.ord,
			:G, 'g'.ord,
			:H, 'h'.ord,
			:I, 'i'.ord,
			:J, 'j'.ord,
			:K, 'k'.ord,
			:L, 'l'.ord,
			:M, 'm'.ord,
			:N, 'n'.ord,
			:O, 'o'.ord,
			:P, 'p'.ord,
			:Q, 'q'.ord,
			:R, 'r'.ord,
			:S, 's'.ord,
			:T, 't'.ord,
			:U, 'u'.ord,
			:V, 'v'.ord,
			:W, 'w'.ord,
			:X, 'x'.ord,
			:Y, 'y'.ord,
			:Z, 'z'.ord,
			:CAPSLOCK, Scancode[:CAPSLOCK] | (1 << 30),
			:F1, Scancode[:F1] | (1 << 30),
			:F2, Scancode[:F2] | (1 << 30),
			:F3, Scancode[:F3] | (1 << 30),
			:F4, Scancode[:F4] | (1 << 30),
			:F5, Scancode[:F5] | (1 << 30),
			:F6, Scancode[:F6] | (1 << 30),
			:F7, Scancode[:F7] | (1 << 30),
			:F8, Scancode[:F8] | (1 << 30),
			:F9, Scancode[:F9] | (1 << 30),
			:F10, Scancode[:F10] | (1 << 30),
			:F11, Scancode[:F11] | (1 << 30),
			:F12, Scancode[:F12] | (1 << 30),
			:PRINTSCREEN, Scancode[:PRINTSCREEN] | (1 << 30),
			:SCROLLLOCK, Scancode[:SCROLLLOCK] | (1 << 30),
			:PAUSE, Scancode[:PAUSE] | (1 << 30),
			:INSERT, Scancode[:INSERT] | (1 << 30),
			:HOME, Scancode[:HOME] | (1 << 30),
			:PAGEUP, Scancode[:PAGEUP] | (1 << 30),
			:DELETE, '\177'.ord,
			:END, Scancode[:END] | (1 << 30),
			:PAGEDOWN, Scancode[:PAGEDOWN] | (1 << 30),
			:RIGHT, Scancode[:RIGHT] | (1 << 30),
			:LEFT, Scancode[:LEFT] | (1 << 30),
			:DOWN, Scancode[:DOWN] | (1 << 30),
			:UP, Scancode[:UP] | (1 << 30),
			:NUMLOCKCLEAR, Scancode[:NUMLOCKCLEAR] | (1 << 30),
			:KP_DIVIDE, Scancode[:KP_DIVIDE] | (1 << 30),
			:KP_MULTIPLY, Scancode[:KP_MULTIPLY] | (1 << 30),
			:KP_MINUS, Scancode[:KP_MINUS] | (1 << 30),
			:KP_PLUS, Scancode[:KP_PLUS] | (1 << 30),
			:KP_ENTER, Scancode[:KP_ENTER] | (1 << 30),
			:KP_1, Scancode[:KP_1] | (1 << 30),
			:KP_2, Scancode[:KP_2] | (1 << 30),
			:KP_3, Scancode[:KP_3] | (1 << 30),
			:KP_4, Scancode[:KP_4] | (1 << 30),
			:KP_5, Scancode[:KP_5] | (1 << 30),
			:KP_6, Scancode[:KP_6] | (1 << 30),
			:KP_7, Scancode[:KP_7] | (1 << 30),
			:KP_8, Scancode[:KP_8] | (1 << 30),
			:KP_9, Scancode[:KP_9] | (1 << 30),
			:KP_0, Scancode[:KP_0] | (1 << 30),
			:KP_PERIOD, Scancode[:KP_PERIOD] | (1 << 30),
			:APPLICATION, Scancode[:APPLICATION] | (1 << 30),
			:POWER, Scancode[:POWER] | (1 << 30),
			:KP_EQUALS, Scancode[:KP_EQUALS] | (1 << 30),
			:F13, Scancode[:F13] | (1 << 30),
			:F14, Scancode[:F14] | (1 << 30),
			:F15, Scancode[:F15] | (1 << 30),
			:F16, Scancode[:F16] | (1 << 30),
			:F17, Scancode[:F17] | (1 << 30),
			:F18, Scancode[:F18] | (1 << 30),
			:F19, Scancode[:F19] | (1 << 30),
			:F20, Scancode[:F20] | (1 << 30),
			:F21, Scancode[:F21] | (1 << 30),
			:F22, Scancode[:F22] | (1 << 30),
			:F23, Scancode[:F23] | (1 << 30),
			:F24, Scancode[:F24] | (1 << 30),
			:EXECUTE, Scancode[:EXECUTE] | (1 << 30),
			:HELP, Scancode[:HELP] | (1 << 30),
			:MENU, Scancode[:MENU] | (1 << 30),
			:SELECT, Scancode[:SELECT] | (1 << 30),
			:STOP, Scancode[:STOP] | (1 << 30),
			:AGAIN, Scancode[:AGAIN] | (1 << 30),
			:UNDO, Scancode[:UNDO] | (1 << 30),
			:CUT, Scancode[:CUT] | (1 << 30),
			:COPY, Scancode[:COPY] | (1 << 30),
			:PASTE, Scancode[:PASTE] | (1 << 30),
			:FIND, Scancode[:FIND] | (1 << 30),
			:MUTE, Scancode[:MUTE] | (1 << 30),
			:VOLUMEUP, Scancode[:VOLUMEUP] | (1 << 30),
			:VOLUMEDOWN, Scancode[:VOLUMEDOWN] | (1 << 30),
			:KP_COMMA, Scancode[:KP_COMMA] | (1 << 30),
			:KP_EQUALSAS400, Scancode[:KP_EQUALSAS400] | (1 << 30),
			:ALTERASE, Scancode[:ALTERASE] | (1 << 30),
			:SYSREQ, Scancode[:SYSREQ] | (1 << 30),
			:CANCEL, Scancode[:CANCEL] | (1 << 30),
			:CLEAR, Scancode[:CLEAR] | (1 << 30),
			:PRIOR, Scancode[:PRIOR] | (1 << 30),
			:RETURN2, Scancode[:RETURN2] | (1 << 30),
			:SEPARATOR, Scancode[:SEPARATOR] | (1 << 30),
			:OUT, Scancode[:OUT] | (1 << 30),
			:OPER, Scancode[:OPER] | (1 << 30),
			:CLEARAGAIN, Scancode[:CLEARAGAIN] | (1 << 30),
			:CRSEL, Scancode[:CRSEL] | (1 << 30),
			:EXSEL, Scancode[:EXSEL] | (1 << 30),
			:KP_00, Scancode[:KP_00] | (1 << 30),
			:KP_000, Scancode[:KP_000] | (1 << 30),
			:THOUSANDSSEPARATOR, Scancode[:THOUSANDSSEPARATOR] | (1 << 30),
			:DECIMALSEPARATOR, Scancode[:DECIMALSEPARATOR] | (1 << 30),
			:CURRENCYUNIT, Scancode[:CURRENCYUNIT] | (1 << 30),
			:CURRENCYSUBUNIT, Scancode[:CURRENCYSUBUNIT] | (1 << 30),
			:KP_LEFTPAREN, Scancode[:KP_LEFTPAREN] | (1 << 30),
			:KP_RIGHTPAREN, Scancode[:KP_RIGHTPAREN] | (1 << 30),
			:KP_LEFTBRACE, Scancode[:KP_LEFTBRACE] | (1 << 30),
			:KP_RIGHTBRACE, Scancode[:KP_RIGHTBRACE] | (1 << 30),
			:KP_TAB, Scancode[:KP_TAB] | (1 << 30),
			:KP_BACKSPACE, Scancode[:KP_BACKSPACE] | (1 << 30),
			:KP_A, Scancode[:KP_A] | (1 << 30),
			:KP_B, Scancode[:KP_B] | (1 << 30),
			:KP_C, Scancode[:KP_C] | (1 << 30),
			:KP_D, Scancode[:KP_D] | (1 << 30),
			:KP_E, Scancode[:KP_E] | (1 << 30),
			:KP_F, Scancode[:KP_F] | (1 << 30),
			:KP_XOR, Scancode[:KP_XOR] | (1 << 30),
			:KP_POWER, Scancode[:KP_POWER] | (1 << 30),
			:KP_PERCENT, Scancode[:KP_PERCENT] | (1 << 30),
			:KP_LESS, Scancode[:KP_LESS] | (1 << 30),
			:KP_GREATER, Scancode[:KP_GREATER] | (1 << 30),
			:KP_AMPERSAND, Scancode[:KP_AMPERSAND] | (1 << 30),
			:KP_DBLAMPERSAND, Scancode[:KP_DBLAMPERSAND] | (1 << 30),
			:KP_VERTICALBAR, Scancode[:KP_VERTICALBAR] | (1 << 30),
			:KP_DBLVERTICALBAR, Scancode[:KP_DBLVERTICALBAR] | (1 << 30),
			:KP_COLON, Scancode[:KP_COLON] | (1 << 30),
			:KP_HASH, Scancode[:KP_HASH] | (1 << 30),
			:KP_SPACE, Scancode[:KP_SPACE] | (1 << 30),
			:KP_AT, Scancode[:KP_AT] | (1 << 30),
			:KP_EXCLAM, Scancode[:KP_EXCLAM] | (1 << 30),
			:KP_MEMSTORE, Scancode[:KP_MEMSTORE] | (1 << 30),
			:KP_MEMRECALL, Scancode[:KP_MEMRECALL] | (1 << 30),
			:KP_MEMCLEAR, Scancode[:KP_MEMCLEAR] | (1 << 30),
			:KP_MEMADD, Scancode[:KP_MEMADD] | (1 << 30),
			:KP_MEMSUBTRACT, Scancode[:KP_MEMSUBTRACT] | (1 << 30),
			:KP_MEMMULTIPLY, Scancode[:KP_MEMMULTIPLY] | (1 << 30),
			:KP_MEMDIVIDE, Scancode[:KP_MEMDIVIDE] | (1 << 30),
			:KP_PLUSMINUS, Scancode[:KP_PLUSMINUS] | (1 << 30),
			:KP_CLEAR, Scancode[:KP_CLEAR] | (1 << 30),
			:KP_CLEARENTRY, Scancode[:KP_CLEARENTRY] | (1 << 30),
			:KP_BINARY, Scancode[:KP_BINARY] | (1 << 30),
			:KP_OCTAL, Scancode[:KP_OCTAL] | (1 << 30),
			:KP_DECIMAL, Scancode[:KP_DECIMAL] | (1 << 30),
			:KP_HEXADECIMAL, Scancode[:KP_HEXADECIMAL] | (1 << 30),
			:LCTRL, Scancode[:LCTRL] | (1 << 30),
			:LSHIFT, Scancode[:LSHIFT] | (1 << 30),
			:LALT, Scancode[:LALT] | (1 << 30),
			:LGUI, Scancode[:LGUI] | (1 << 30),
			:RCTRL, Scancode[:RCTRL] | (1 << 30),
			:RSHIFT, Scancode[:RSHIFT] | (1 << 30),
			:RALT, Scancode[:RALT] | (1 << 30),
			:RGUI, Scancode[:RGUI] | (1 << 30),
			:MODE, Scancode[:MODE] | (1 << 30),
			:AUDIONEXT, Scancode[:AUDIONEXT] | (1 << 30),
			:AUDIOPREV, Scancode[:AUDIOPREV] | (1 << 30),
			:AUDIOSTOP, Scancode[:AUDIOSTOP] | (1 << 30),
			:AUDIOPLAY, Scancode[:AUDIOPLAY] | (1 << 30),
			:AUDIOMUTE, Scancode[:AUDIOMUTE] | (1 << 30),
			:MEDIASELECT, Scancode[:MEDIASELECT] | (1 << 30),
			:WWW, Scancode[:WWW] | (1 << 30),
			:MAIL, Scancode[:MAIL] | (1 << 30),
			:CALCULATOR, Scancode[:CALCULATOR] | (1 << 30),
			:COMPUTER, Scancode[:COMPUTER] | (1 << 30),
			:AC_SEARCH, Scancode[:AC_SEARCH] | (1 << 30),
			:AC_HOME, Scancode[:AC_HOME] | (1 << 30),
			:AC_BACK, Scancode[:AC_BACK] | (1 << 30),
			:AC_FORWARD, Scancode[:AC_FORWARD] | (1 << 30),
			:AC_STOP, Scancode[:AC_STOP] | (1 << 30),
			:AC_REFRESH, Scancode[:AC_REFRESH] | (1 << 30),
			:AC_BOOKMARKS, Scancode[:AC_BOOKMARKS] | (1 << 30),
			:BRIGHTNESSDOWN, Scancode[:BRIGHTNESSDOWN] | (1 << 30),
			:BRIGHTNESSUP, Scancode[:BRIGHTNESSUP] | (1 << 30),
			:DISPLAYSWITCH, Scancode[:DISPLAYSWITCH] | (1 << 30),
			:KBDILLUMTOGGLE, Scancode[:KBDILLUMTOGGLE] | (1 << 30),
			:KBDILLUMDOWN, Scancode[:KBDILLUMDOWN] | (1 << 30),
			:KBDILLUMUP, Scancode[:KBDILLUMUP] | (1 << 30),
			:EJECT, Scancode[:EJECT] | (1 << 30),
			:SLEEP, Scancode[:SLEEP] | (1 << 30),
			:APP1, Scancode[:APP1] | (1 << 30),
			:APP2, Scancode[:APP2] | (1 << 30),
			:AUDIOREWIND, Scancode[:AUDIOREWIND] | (1 << 30),
			:AUDIOFASTFORWARD, Scancode[:AUDIOFASTFORWARD] | (1 << 30)
		]

		Keymod = bitmask FFI::TYPE_UINT16, :Keymod, [
			:lshift, 0,
			:rshift, 1,
			:lctrl, 6,
			:rctrl, 7,
			:lalt, 8,
			:ralt, 9,
			:lgui, 10,
			:rgui, 11,
			:num, 12,
			:caps, 13,
			:mode, 14,
		]
	end
end
