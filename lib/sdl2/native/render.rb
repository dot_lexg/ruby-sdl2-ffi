# frozen_string_literal: true

require_relative 'pixels'
require_relative 'rect'
require_relative 'surface'
module SDL2
	# @!group SDL_render.h
	# @api native
	module Native
		RENDERER_PTR = :pointer
		TEXTURE_PTR = :pointer

		# Enumerations:
		# SDL_BlendFactor
		# SDL_BlendOperation
		RendererFlags = bitmask FFI::TYPE_UINT32, :RendererFlags, [
			:software, 0,
			:accelerated, 1,
			:presentvsync, 2,
			:targettexture, 3
		]
		# SDL_RendererFlip
		TextureAccess = enum :SDL_TextureAccess, [
			:static, :streaming, :target
		]
		# SDL_TextureModulate

		# Structures:
		class RendererInfo < FFI::Struct
			layout(
				name: :string,
				flags: RendererFlags,
				num_texture_formats: :uint32,
				texture_formats: [PixelFormatEnum, 16],
				max_texture_width: :int,
				max_texture_height: :int
			)
		end

		# attach_function :compose_custom_blend_mode, :SDL_ComposeCustomBlendMode

		# @!method self.create_renderer(window, index, flags)
		# @param window [SDL_Window *] the window where rendering is displayed
		# @param index [Integer] the index of the rendering driver to initialize, or -1 to initialize the first one supporting the requested flags
		# @param flags [Integer] 0, or one or more SDL_RendererFlags OR'd together; see Remarks for details
		# @return [SDL_Renderer *] FFI::Pointer::NULL on failure (see {.get_error} or use {.check})
		attach_function :create_renderer, :SDL_CreateRenderer, [:pointer, :int, RendererFlags], :pointer

		# attach_function :create_software_renderer, :SDL_CreateSoftwareRenderer

		# @!method self.create_texture(renderer, format, access, w, h)
		# @param renderer [SDL_Renderer *]
		# @param format [PixelFormatEnum]
		# @param access [TextureAccess]
		# @param w [Integer]
		# @param h [Integer]
		# @return [SDL_Texture *] FFI::Pointer::NULL on failure (see {.get_error} or use {.check})
		attach_function :create_texture, :SDL_CreateTexture, [RENDERER_PTR, PixelFormatEnum, TextureAccess, :int, :int], TEXTURE_PTR

		# @!method self.create_texture_from_surface(renderer, surface)
		attach_function :create_texture_from_surface, :SDL_CreateTextureFromSurface, [RENDERER_PTR, Surface.ptr], TEXTURE_PTR

		# attach_function :create_window_and_renderer, :SDL_CreateWindowAndRenderer # call {.create_window} and {.create_renderer} seperately?

		# @!method self.destroy_renderer(renderer)
		# @param renderer [SDL_Renderer *]
		# @return [nil]
		attach_function :destroy_renderer, :SDL_DestroyRenderer, [:pointer], :void


		# @!method self.destroy_texture(texture)
		# @param texture [SDL_Texture *]
		# @return [nil]
		attach_function :destroy_texture, :SDL_DestroyTexture, [:pointer], :void


		# attach_function :gl__bind_texture, :SDL_GL_BindTexture
		# attach_function :gl__unbind_texture, :SDL_GL_UnbindTexture
		# attach_function :get_num_render_drivers, :SDL_GetNumRenderDrivers
		# attach_function :get_render_draw_blend_mode, :SDL_GetRenderDrawBlendMode
		# attach_function :get_render_draw_color, :SDL_GetRenderDrawColor
		# attach_function :get_render_driver_info, :SDL_GetRenderDriverInfo
		# attach_function :get_render_target, :SDL_GetRenderTarget

		# @!method self.get_renderer(window)
		# @param window [SDL_Window *]
		# @return [SDL_Renderer *] FFI::Pointer::NULL if there is no renderer. (contrary to wiki docs, which instructs checking {.get_error})
		attach_function :get_renderer, :SDL_GetRenderer, [:pointer], :pointer

		# @!method self.get_renderer_info(renderer, renderer_info)
		# @param renderer [SDL_Renderer *]
		# @param renderer_info [out SDL_]
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :get_renderer_info, :SDL_GetRendererInfo, [:pointer, RendererInfo.ptr(:out)], :int

		# attach_function :get_renderer_output_size, :SDL_GetRendererOutputSize
		# attach_function :get_texture_alpha_mod, :SDL_GetTextureAlphaMod
		# attach_function :get_texture_blend_mode, :SDL_GetTextureBlendMode
		# attach_function :get_texture_color_mod, :SDL_GetTextureColorMod

		# @!method self.lock_texture(texture, rect, pixels, pitch)
		# @param texture [SDL_Texture *]
		# @param rect [SDL_Rect *]
		# @param pixels [out FFI::Pointer *]
		# @param pitch [out Integer *]
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :lock_texture, :SDL_LockTexture, [:pointer, :pointer, :pointer, :pointer], :int

		# @!method self.query_texture(texture, format, access, w, h)
		# @param texture [SDL_Texture *]
		# @param format [out PixelFormatEnum *]
		# @param access [out TextureAccess *]
		# @param w [out int *]
		# @param h [out int *]
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :query_texture, :SDL_QueryTexture, [TEXTURE_PTR, PixelFormatEnumPtr.out, IntPtr.out, IntPtr.out, IntPtr.out], :int

		# @!method self.render_clear(renderer)
		# @param renderer [SDL_Renderer *]
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :render_clear, :SDL_RenderClear, [:pointer], :int

		# @!method self.render_copy(renderer, texture, srcrect, dstrect)
		# @param renderer [SDL_Renderer *]
		# @param texture [SDL_Texture *]
		# @param srcrect [SDL_Rect *]
		# @param dstrect [SDL_Rect *]
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :render_copy, :SDL_RenderCopy, [:pointer, :pointer, Rect.ptr(:in), Rect.ptr(:in)], :int

		# attach_function :render_copy_ex, :SDL_RenderCopyEx
		# attach_function :render_draw_line, :SDL_RenderDrawLine
		# attach_function :render_draw_lines, :SDL_RenderDrawLines
		# attach_function :render_draw_point, :SDL_RenderDrawPoint
		# attach_function :render_draw_points, :SDL_RenderDrawPoints
		# attach_function :render_draw_rect, :SDL_RenderDrawRect
		# attach_function :render_draw_rects, :SDL_RenderDrawRects

		# @!method self.render_fill_rect(renderer, rect)
		# @param renderer [SDL_Renderer *]
		# @param rect [SDL_Rect *]
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :render_fill_rect, :SDL_RenderFillRect, [:pointer, Rect.ptr(:in)], :int

		# attach_function :render_fill_rects, :SDL_RenderFillRects
		# attach_function :render_get_clip_rect, :SDL_RenderGetClipRect
		# attach_function :render_get_integer_scale, :SDL_RenderGetIntegerScale
		# attach_function :render_get_logical_size, :SDL_RenderGetLogicalSize
		# attach_function :render_get_scale, :SDL_RenderGetScale
		# attach_function :render_get_viewport, :SDL_RenderGetViewport
		# attach_function :render_is_clip_enabled, :SDL_RenderIsClipEnabled

		# @!method self.render_present(renderer)
		# @param renderer [SDL_Renderer *]
		# @return [nil]
		attach_function :render_present, :SDL_RenderPresent, [:pointer], :void

		# attach_function :render_read_pixels, :SDL_RenderReadPixels
		# attach_function :render_set_clip_rect, :SDL_RenderSetClipRect
		# attach_function :render_set_integer_scale, :SDL_RenderSetIntegerScale
		# attach_function :render_set_logical_size, :SDL_RenderSetLogicalSize
		# attach_function :render_set_scale, :SDL_RenderSetScale
		# attach_function :render_set_viewport, :SDL_RenderSetViewport
		# attach_function :render_target_supported, :SDL_RenderTargetSupported
		# attach_function :set_render_draw_blend_mode, :SDL_SetRenderDrawBlendMode

		# @!method self.set_render_draw_color(renderer, r, g, b, a)
		# @param renderer [SDL_Renderer *]
		# @param r [Integer] the red value used to draw on the rendering target
		# @param g [Integer] the green value used to draw on the rendering target
		# @param b [Integer] the blue value used to draw on the rendering target
		# @param a [Integer] the alpha value used to draw on the rendering target; usually SDL2::Native::ALPHA_OPAQUE (255).
		#   Use {.set_render_draw_blend_mode} to specify how the alpha channel is used.
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :set_render_draw_color, :SDL_SetRenderDrawColor, [:pointer, :uint8, :uint8, :uint8, :uint8], :int

		# attach_function :set_render_target, :SDL_SetRenderTarget
		# attach_function :set_texture_alpha_mod, :SDL_SetTextureAlphaMod
		# attach_function :set_texture_blend_mode, :SDL_SetTextureBlendMode
		# attach_function :set_texture_color_mod, :SDL_SetTextureColorMod

		# @!method self.unlock_texture(texture)
		# @param texture [SDL_Texture *]
		# @return [nil]
		attach_function :unlock_texture, :SDL_UnlockTexture, [:pointer], :void

		# @!method self.update_texture(texture, rect, pixels, pitch)
		# @param texture [SDL_Texture *]
		# @param rect [SDL_Rect *]
		# @param pixels [FFI::Pointer]
		# @param pitch [Integer]
		# @return [Integer] 0 on success, < 0 on error (see {.get_error} or use {.check})
		attach_function :update_texture, :SDL_UpdateTexture, [:pointer, Rect.ptr(:in), :buffer_in, :int], :int

		# attach_function :update_yuvtexture, :SDL_UpdateYUVTexture
	end
end
