# frozen_string_literal: true

# The interface and vast majority of the documentation here is adapted from
# https://wiki.libsdl.org/ and https://github.com/libsdl-org/sdl.
# See the /reference/ folder for specific position in history that was used to
# build this FFI.

module SDL2
	# @!group SDL_video.h
	# @api native
	module Native
		# These are the currently supported flags for {Surface}. Used internally
		# (read-only).
		SurfaceFlags = bitmask :SurfaceFlags, [
			:prealloc,     0,
			:rleaccel,     1,
			:dontfree,     2,
			:simd_aligned, 3
		]

		def self.must_lock(surface)
			surface[:flags].include?(:rleaccel)
		end

		# A collection of pixels used in software blitting. This structure should be
		# treated as read-only, except for +pixels+, which, if not NULL, contains
		# the raw pixel data for the surface.
		class Surface < FFI::Struct
			layout(
				flags: :uint32,          # (Read-only)
				format: PixelFormat.ptr, # (Read-only)
				w: :int,                 # (Read-only)
				h: :int,                 # (Read-only)
				pitch: :int,             # (Read-only)
				pixels: :pointer,        # (Read-write)
				userdata: :pointer,      # (Read-write)  Application data associated with the surface
				locked: :int,            # (Read-only)   Information needed for surfaces requiring locks
				lock_data: :pointer,     # (Read-only)
				clip_rect: Rect,         # (Read-only)   Clipping information
				map: :pointer,           # (Private Blitmap *) Info for fast blit mapping to other surfaces
				refcount: :int           # (Read-mostly) Reference count -- used when freeing surface
			)
		end

		# The type of function used for surface blitting functions.
		# (src, srcrect, dst, dstrect)
		callback :blit, [Surface.ptr, Rect.ptr, Surface.ptr, Rect.ptr], :int

		# The formula used for converting between YUV and RGB
		YuvConversionMode = enum :YuvConversionMode, [
			:jpeg,      # Full range JPEG
			:bt601,     # BT.601 (the default)
			:bt709,     # BT.709
			:automatic  # BT.601 for SD content, BT.709 for HD content
		]

		#  Allocate and free an RGB surface.
		#
		#  If the depth is 4 or 8 bits, an empty palette is allocated for the surface.
		#  If the depth is greater than 8 bits, the pixel format is set using the
		#  flags '[RGB]mask'.
		#
		#  If the function runs out of memory, it will return NULL.
		#
		#  \param flags The \c flags are obsolete and should be set to 0.
		#  \param width The width in pixels of the surface to create.
		#  \param height The height in pixels of the surface to create.
		#  \param depth The depth in bits of the surface to create.
		#  \param Rmask The red mask of the surface to create.
		#  \param Gmask The green mask of the surface to create.
		#  \param Bmask The blue mask of the surface to create.
		#  \param Amask The alpha mask of the surface to create.
		# @!method self.create_rgb_surface(flags, width, height, depth, r_mask, g_mask, b_mask, a_mask)
		attach_function :create_rgb_surface, :SDL_CreateRGBSurface, [:uint32, :int, :int, :int, :uint32, :uint32, :uint32, :uint32], Surface.ptr(:out)


		# FIXME: for 2.1: why does this ask for depth? Format provides that.
		# @!method self.create_rgb_surface_with_format(flags, width, height, depth, format)
		attach_function :create_rgb_surface_with_format, :SDL_CreateRGBSurfaceWithFormat, [:uint32, :int, :int, :int, :uint32], Surface.ptr(:out)

		# @!method self.create_rgb_surface_from(pixels, width, height, depth, pitch, r_mask, g_mask, b_mask, a_mask)
		attach_function :create_rgb_surface_from, :SDL_CreateRGBSurfaceFrom,
			[:pointer, :int, :int, :int, :int, :uint32, :uint32, :uint32, :uint32], Surface.ptr(:out)

		# @!method self.create_rgb_surface_with_format_from(pixels, width, height, depth, pitch, format)
		attach_function :create_rgb_surface_with_format_from, :SDL_CreateRGBSurfaceWithFormatFrom,
			[:pointer, :int, :int, :int, :int, :uint32], Surface.ptr(:out)

		# @!method self.free_surface(surface)
		attach_function :free_surface, :SDL_FreeSurface, [Surface.ptr(:in)], :void

		# @!method self.free_surface_ptr(surface)
		attach_function :free_surface_ptr, :SDL_FreeSurface, [:pointer], :void

		# #  Set the palette used by a surface.
		# #
		# #  A single palette can be shared with many surfaces.
		# #
		# #  \return 0, or -1 if the surface format doesn't use a palette.
		# extern DECLSPEC :int SDLCALL SDL_SetSurfacePalette(SDL_Surface * surface,
		# 																									SDL_Palette * palette);

		# #  \brief Sets up a surface for directly accessing the pixels.
		# #
		# #  Between calls to SDL_LockSurface() / SDL_UnlockSurface(), you can write
		# #  to and read from \c surface->pixels, using the pixel format stored in
		# #  \c surface->format.  Once you are done accessing the surface, you should
		# #  use SDL_UnlockSurface() to release it.
		# #
		# #  Not all surfaces require locking.  If SDL_MUSTLOCK(surface) evaluates
		# #  to 0, then you can read and write to the surface at any time, and the
		# #  pixel format of the surface will not change.
		# #
		# #  No operating system or library calls should be made between lock/unlock
		# #  pairs, as critical system locks may be held during this time.
		# #
		# #  SDL_LockSurface() returns 0, or -1 if the surface couldn't be locked.
		# #
		# #  \sa SDL_UnlockSurface()
		# extern DECLSPEC :int SDLCALL SDL_LockSurface(SDL_Surface * surface);

		# # \sa SDL_LockSurface()
		# extern DECLSPEC void SDLCALL SDL_UnlockSurface(SDL_Surface * surface);

		# #  Load a surface from a seekable SDL data stream (memory or file).
		# #
		# #  If \c freesrc is non-zero, the stream will be closed after being read.
		# #
		# #  The new surface should be freed with SDL_FreeSurface().
		# #
		# #  \return the new surface, or NULL if there was an error.
		# extern DECLSPEC SDL_Surface *SDLCALL SDL_LoadBMP_RW(SDL_RWops * src,
		# 																										:int freesrc);

		# #  Load a surface from a file.
		# #
		# #  Convenience macro.
		# #define SDL_LoadBMP(file)   SDL_LoadBMP_RW(SDL_RWFromFile(file, "rb"), 1)

		# #  Save a surface to a seekable SDL data stream (memory or file).
		# #
		# #  Surfaces with a 24-bit, 32-bit and paletted 8-bit format get saved in the
		# #  BMP directly. Other RGB formats with 8-bit or higher get converted to a
		# #  24-bit surface or, if they have an alpha mask or a colorkey, to a 32-bit
		# #  surface before they are saved. YUV and paletted 1-bit and 4-bit formats are
		# #  not supported.
		# #
		# #  If \c freedst is non-zero, the stream will be closed after being written.
		# #
		# #  \return 0 if successful or -1 if there was an error.
		# extern DECLSPEC :int SDLCALL SDL_SaveBMP_RW
		# 		(SDL_Surface * surface, SDL_RWops * dst, :int freedst);

		# #  Save a surface to a file.
		# #
		# #  Convenience macro.
		# #define SDL_SaveBMP(surface, file) \
		# 				SDL_SaveBMP_RW(surface, SDL_RWFromFile(file, "wb"), 1)

		# #  \brief Sets the RLE acceleration hint for a surface.
		# #
		# #  \return 0 on success, or -1 if the surface is not valid
		# #
		# #  \note If RLE is enabled, colorkey and alpha blending blits are much faster,
		# #        but the surface must be locked before directly accessing the pixels.
		# extern DECLSPEC :int SDLCALL SDL_SetSurfaceRLE(SDL_Surface * surface,
		# 																							:int flag);

		# #  \brief Sets the color key (transparent pixel) in a blittable surface.
		# #
		# #  \param surface The surface to update
		# #  \param flag Non-zero to enable colorkey and 0 to disable colorkey
		# #  \param key The transparent pixel in the native surface format
		# #
		# #  \return 0 on success, or -1 if the surface is not valid
		# #
		# #  You can pass SDL_RLEACCEL to enable RLE accelerated blits.
		# extern DECLSPEC :int SDLCALL SDL_SetColorKey(SDL_Surface * surface,
		# 																						:int flag, :uint32 key);

		# #  \brief Returns whether the surface has a color key
		# #
		# #  \return SDL_TRUE if the surface has a color key, or SDL_FALSE if the surface is NULL or has no color key
		# extern DECLSPEC SDL_bool SDLCALL SDL_HasColorKey(SDL_Surface * surface);

		# #  \brief Gets the color key (transparent pixel) in a blittable surface.
		# #
		# #  \param surface The surface to update
		# #  \param key A pointer filled in with the transparent pixel in the native
		# #             surface format
		# #
		# #  \return 0 on success, or -1 if the surface is not valid or colorkey is not
		# #          enabled.
		# extern DECLSPEC :int SDLCALL SDL_GetColorKey(SDL_Surface * surface,
		# 																						:uint32 * key);

		# #  \brief Set an additional color value used in blit operations.
		# #
		# #  \param surface The surface to update.
		# #  \param r The red color value multiplied into blit operations.
		# #  \param g The green color value multiplied into blit operations.
		# #  \param b The blue color value multiplied into blit operations.
		# #
		# #  \return 0 on success, or -1 if the surface is not valid.
		# #
		# #  \sa SDL_GetSurfaceColorMod()
		# extern DECLSPEC :int SDLCALL SDL_SetSurfaceColorMod(SDL_Surface * surface,
		# 																									:uint8 r, :uint8 g, :uint8 b);


		# #  \brief Get the additional color value used in blit operations.
		# #
		# #  \param surface The surface to query.
		# #  \param r A pointer filled in with the current red color value.
		# #  \param g A pointer filled in with the current green color value.
		# #  \param b A pointer filled in with the current blue color value.
		# #
		# #  \return 0 on success, or -1 if the surface is not valid.
		# #
		# #  \sa SDL_SetSurfaceColorMod()
		# extern DECLSPEC :int SDLCALL SDL_GetSurfaceColorMod(SDL_Surface * surface,
		# 																									:uint8 * r, :uint8 * g,
		# 																									:uint8 * b);

		# #  \brief Set an additional alpha value used in blit operations.
		# #
		# #  \param surface The surface to update.
		# #  \param alpha The alpha value multiplied into blit operations.
		# #
		# #  \return 0 on success, or -1 if the surface is not valid.
		# #
		# #  \sa SDL_GetSurfaceAlphaMod()
		# extern DECLSPEC :int SDLCALL SDL_SetSurfaceAlphaMod(SDL_Surface * surface,
		# 																									:uint8 alpha);

		# #  \brief Get the additional alpha value used in blit operations.
		# #
		# #  \param surface The surface to query.
		# #  \param alpha A pointer filled in with the current alpha value.
		# #
		# #  \return 0 on success, or -1 if the surface is not valid.
		# #
		# #  \sa SDL_SetSurfaceAlphaMod()
		# extern DECLSPEC :int SDLCALL SDL_GetSurfaceAlphaMod(SDL_Surface * surface,
		# 																									:uint8 * alpha);

		# #  \brief Set the blend mode used for blit operations.
		# #
		# #  \param surface The surface to update.
		# #  \param blendMode ::SDL_BlendMode to use for blit blending.
		# #
		# #  \return 0 on success, or -1 if the parameters are not valid.
		# #
		# #  \sa SDL_GetSurfaceBlendMode()
		# extern DECLSPEC :int SDLCALL SDL_SetSurfaceBlendMode(SDL_Surface * surface,
		# 																										SDL_BlendMode blendMode);

		# #  \brief Get the blend mode used for blit operations.
		# #
		# #  \param surface   The surface to query.
		# #  \param blendMode A pointer filled in with the current blend mode.
		# #
		# #  \return 0 on success, or -1 if the surface is not valid.
		# #
		# #  \sa SDL_SetSurfaceBlendMode()
		# extern DECLSPEC :int SDLCALL SDL_GetSurfaceBlendMode(SDL_Surface * surface,
		# 																										SDL_BlendMode *blendMode);

		# #  Sets the clipping rectangle for the destination surface in a blit.
		# #
		# #  If the clip rectangle is NULL, clipping will be disabled.
		# #
		# #  If the clip rectangle doesn't intersect the surface, the function will
		# #  return SDL_FALSE and blits will be completely clipped.  Otherwise the
		# #  function returns SDL_TRUE and blits to the surface will be clipped to
		# #  the intersection of the surface area and the clipping rectangle.
		# #
		# #  Note that blits are automatically clipped to the edges of the source
		# #  and destination surfaces.
		# extern DECLSPEC SDL_bool SDLCALL SDL_SetClipRect(SDL_Surface * surface,
		# 																								const SDL_Rect * rect);

		# #  Gets the clipping rectangle for the destination surface in a blit.
		# #
		# #  \c rect must be a pointer to a valid rectangle which will be filled
		# #  with the correct values.
		# extern DECLSPEC void SDLCALL SDL_GetClipRect(SDL_Surface * surface,
		# 																						SDL_Rect * rect);

		# /*
		# # Creates a new surface identical to the existing surface
		# extern DECLSPEC SDL_Surface *SDLCALL SDL_DuplicateSurface(SDL_Surface * surface);

		# #  Creates a new surface of the specified format, and then copies and maps
		# #  the given surface to it so the blit of the converted surface will be as
		# #  fast as possible.  If this function fails, it returns NULL.
		# #
		# #  The \c flags parameter is passed to SDL_CreateRGBSurface() and has those
		# #  semantics.  You can also pass ::SDL_RLEACCEL in the flags parameter and
		# #  SDL will try to RLE accelerate colorkey and alpha blits in the resulting
		# #  surface.
		# extern DECLSPEC SDL_Surface *SDLCALL SDL_ConvertSurface
		# 		(SDL_Surface * src, const SDL_PixelFormat * fmt, :uint32 flags);
		# extern DECLSPEC SDL_Surface *SDLCALL SDL_ConvertSurfaceFormat
		# 		(SDL_Surface * src, :uint32 pixel_format, :uint32 flags);

		# # \brief Copy a block of pixels of one format to another format
		# #
		# #  \return 0 on success, or -1 if there was an error
		# extern DECLSPEC :int SDLCALL SDL_ConvertPixels(:int width, :int height,
		# 																							:uint32 src_format,
		# 																							const :pointer  src, :int src_pitch,
		# 																							:uint32 dst_format,
		# 																							:pointer  dst, :int dst_pitch);

		# #  Performs a fast fill of the given rectangle with \c color.
		# #
		# #  If \c rect is NULL, the whole surface will be filled with \c color.
		# #
		# #  The color should be a pixel of the format used by the surface, and
		# #  can be generated by the SDL_MapRGB() function.
		# #
		# #  \return 0 on success, or -1 on error.
		# extern DECLSPEC :int SDLCALL SDL_FillRect
		# 		(SDL_Surface * dst, const SDL_Rect * rect, :uint32 color);
		# extern DECLSPEC :int SDLCALL SDL_FillRects
		# 		(SDL_Surface * dst, const SDL_Rect * rects, :int count, :uint32 color);

		# #  Performs a fast blit from the source surface to the destination surface.
		# #
		# #  This assumes that the source and destination rectangles are
		# #  the same size.  If either \c srcrect or \c dstrect are NULL, the entire
		# #  surface (\c src or \c dst) is copied.  The final blit rectangles are saved
		# #  in \c srcrect and \c dstrect after all clipping is performed.
		# #
		# #  \return If the blit is successful, it returns 0, otherwise it returns -1.
		# #
		# #  The blit function should not be called on a locked surface.
		# #
		# #  The blit semantics for surfaces with and without blending and colorkey
		# #  are defined as follows:
		# #  \verbatim
		# 		# RGBA->RGB:
		# 		# 	Source surface blend mode set to SDL_BLENDMODE_BLEND:
		# 		# 		alpha-blend (using the source alpha-channel and per-surface alpha)
		# 		# 		SDL_SRCCOLORKEY ignored.
		# 		# 	Source surface blend mode set to SDL_BLENDMODE_NONE:
		# 		# 		copy RGB.
		# 		# 		if SDL_SRCCOLORKEY set, only copy the pixels matching the
		# 		# 		RGB values of the source color key, ignoring alpha in the
		# 		# 		comparison.

		# 		# RGB->RGBA:
		# 		# 	Source surface blend mode set to SDL_BLENDMODE_BLEND:
		# 		# 		alpha-blend (using the source per-surface alpha)
		# 		# 	Source surface blend mode set to SDL_BLENDMODE_NONE:
		# 		# 		copy RGB, set destination alpha to source per-surface alpha value.
		# 		# 	both:
		# 		# 		if SDL_SRCCOLORKEY set, only copy the pixels matching the
		# 		# 		source color key.

		# 		# RGBA->RGBA:
		# 		# 	Source surface blend mode set to SDL_BLENDMODE_BLEND:
		# 		# 		alpha-blend (using the source alpha-channel and per-surface alpha)
		# 		# 		SDL_SRCCOLORKEY ignored.
		# 		# 	Source surface blend mode set to SDL_BLENDMODE_NONE:
		# 		# 		copy all of RGBA to the destination.
		# 		# 		if SDL_SRCCOLORKEY set, only copy the pixels matching the
		# 		# 		RGB values of the source color key, ignoring alpha in the
		# 		# 		comparison.

		# 		# RGB->RGB:
		# 		# 	Source surface blend mode set to SDL_BLENDMODE_BLEND:
		# 		# 		alpha-blend (using the source per-surface alpha)
		# 		# 	Source surface blend mode set to SDL_BLENDMODE_NONE:
		# 		# 		copy RGB.
		# 		# 	both:
		# 		# 		if SDL_SRCCOLORKEY set, only copy the pixels matching the
		# 		# 		source color key.
		# 		# \endverbatim
		# #
		# #  You should call SDL_BlitSurface() unless you know exactly how SDL
		# #  blitting works internally and how to use the other blit functions.
		# #define SDL_BlitSurface SDL_UpperBlit

		# #  This is the public blit function, SDL_BlitSurface(), and it performs
		# #  rectangle validation and clipping before passing it to SDL_LowerBlit()
		# extern DECLSPEC :int SDLCALL SDL_UpperBlit
		# 		(SDL_Surface * src, const SDL_Rect * srcrect,
		# 		SDL_Surface * dst, SDL_Rect * dstrect);

		# #  This is a semi-private blit function and it performs low-level surface
		# #  blitting only.
		# extern DECLSPEC :int SDLCALL SDL_LowerBlit
		# 		(SDL_Surface * src, SDL_Rect * srcrect,
		# 		SDL_Surface * dst, SDL_Rect * dstrect);

		# #  \brief Perform a fast, low quality, stretch blit between two surfaces of the
		# #         same pixel format.
		# #
		# #  \note This function uses a static buffer, and is not thread-safe.
		# extern DECLSPEC :int SDLCALL SDL_SoftStretch(SDL_Surface * src,
		# 																						const SDL_Rect * srcrect,
		# 																						SDL_Surface * dst,
		# 																						const SDL_Rect * dstrect);

		# #define SDL_BlitScaled SDL_UpperBlitScaled

		# #  This is the public scaled blit function, SDL_BlitScaled(), and it performs
		# #  rectangle validation and clipping before passing it to SDL_LowerBlitScaled()
		# extern DECLSPEC :int SDLCALL SDL_UpperBlitScaled
		# 		(SDL_Surface * src, const SDL_Rect * srcrect,
		# 		SDL_Surface * dst, SDL_Rect * dstrect);

		# #  This is a semi-private blit function and it performs low-level surface
		# #  scaled blitting only.
		# extern DECLSPEC :int SDLCALL SDL_LowerBlitScaled
		# 		(SDL_Surface * src, SDL_Rect * srcrect,
		# 		SDL_Surface * dst, SDL_Rect * dstrect);

		# #  \brief Set the YUV conversion mode
		# extern DECLSPEC void SDLCALL SDL_SetYUVConversionMode(SDL_YUV_CONVERSION_MODE mode);

		# #  \brief Get the YUV conversion mode
		# extern DECLSPEC SDL_YUV_CONVERSION_MODE SDLCALL SDL_GetYUVConversionMode(void);

		# #  \brief Get the YUV conversion mode, returning the correct mode for the resolution when the current conversion mode is SDL_YUV_CONVERSION_AUTOMATIC
		# extern DECLSPEC SDL_YUV_CONVERSION_MODE SDLCALL SDL_GetYUVConversionModeForResolution(:int width, :int height);

	end
end
