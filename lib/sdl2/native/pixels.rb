# frozen_string_literal: true

module SDL2
	# @!group SDL_pixels.h
	# @api native
	module Native
		PixelType = enum :PixelType, [
			:unknown,
			:index1, :index4, :index8,
			:packed8, :packed16, :packed32,
			:arrayu8, :arrayu16, :arrayu32,
			:arrayf16, :arrayf32
		]
		BitmapOrder = enum :BitmapOrder, [
			:none,
			:order_4321,
			:order_1234
		]
		PackedOrder = enum :PackedOrder, [
			:none,
			:xrgb,
			:rgbx,
			:argb,
			:rgba,
			:xbgr,
			:bgrx,
			:abgr,
			:bgra
		]
		ArrayOrder = enum :ArrayOrder, [
			:none,
			:rgb,
			:rgba,
			:argb,
			:bgr,
			:bgra,
			:abgr
		]
		PackedLayout = enum :PackedLayout, [
			:none,
			:layout_332,
			:layout_4444,
			:layout_1555,
			:layout_5551,
			:layout_565,
			:layout_8888,
			:layout_2101010,
			:layout_1010102
		]

		def self.define_pixelfourcc(str)
			raise if str.bytesize != 4

			a, b, c, d = str.bytes
			a | b << 8 | c << 16 | d << 24
		end

		def self.define_pixelformat(type, order, layout, bits, bytes)
			(1 << 28) | (PixelType[type] << 24) | (order << 20) | (PackedLayout[layout] << 16) | (bits << 8) | (bytes << 0)
		end

		PixelFormatEnum = enum FFI::TYPE_UINT32, :PixelFormatEnum, [
			:unknown,
			:index1lsb,   define_pixelformat(:index1,   BitmapOrder[:order_4321], :none,            1, 0),
			:index1msb,   define_pixelformat(:index1,   BitmapOrder[:order_1234], :none,            1, 0),
			:index4lsb,   define_pixelformat(:index4,   BitmapOrder[:order_4321], :none,            4, 0),
			:index4msb,   define_pixelformat(:index4,   BitmapOrder[:order_1234], :none,            4, 0),
			:index8,      define_pixelformat(:index8,   BitmapOrder[:none],       :none,            8, 1),
			:rgb332,      define_pixelformat(:packed8,  PackedOrder[:xrgb],       :layout_332,      8, 1),
			:xrgb4444,    define_pixelformat(:packed16, PackedOrder[:xrgb],       :layout_4444,    12, 2),
			:rgb444,      define_pixelformat(:packed16, PackedOrder[:xrgb],       :layout_4444,    12, 2),
			:xbgr4444,    define_pixelformat(:packed16, PackedOrder[:xbgr],       :layout_4444,    12, 2),
			:bgr444,      define_pixelformat(:packed16, PackedOrder[:xbgr],       :layout_4444,    12, 2),
			:xrgb1555,    define_pixelformat(:packed16, PackedOrder[:xrgb],       :layout_1555,    15, 2),
			:rgb555,      define_pixelformat(:packed16, PackedOrder[:xrgb],       :layout_1555,    15, 2),
			:xbgr1555,    define_pixelformat(:packed16, PackedOrder[:xbgr],       :layout_1555,    15, 2),
			:bgr555,      define_pixelformat(:packed16, PackedOrder[:xbgr],       :layout_1555,    15, 2),
			:argb4444,    define_pixelformat(:packed16, PackedOrder[:argb],       :layout_4444,    16, 2),
			:rgba4444,    define_pixelformat(:packed16, PackedOrder[:rgba],       :layout_4444,    16, 2),
			:abgr4444,    define_pixelformat(:packed16, PackedOrder[:abgr],       :layout_4444,    16, 2),
			:bgra4444,    define_pixelformat(:packed16, PackedOrder[:bgra],       :layout_4444,    16, 2),
			:argb1555,    define_pixelformat(:packed16, PackedOrder[:argb],       :layout_1555,    16, 2),
			:rgba5551,    define_pixelformat(:packed16, PackedOrder[:rgba],       :layout_5551,    16, 2),
			:abgr1555,    define_pixelformat(:packed16, PackedOrder[:abgr],       :layout_1555,    16, 2),
			:bgra5551,    define_pixelformat(:packed16, PackedOrder[:bgra],       :layout_5551,    16, 2),
			:rgb565,      define_pixelformat(:packed16, PackedOrder[:xrgb],       :layout_565,     16, 2),
			:bgr565,      define_pixelformat(:packed16, PackedOrder[:xbgr],       :layout_565,     16, 2),
			:rgb24,       define_pixelformat(:arrayu8,  ArrayOrder[:rgb],         :none,           24, 3),
			:bgr24,       define_pixelformat(:arrayu8,  ArrayOrder[:bgr],         :none,           24, 3),
			:xrgb8888,    define_pixelformat(:packed32, PackedOrder[:xrgb],       :layout_8888,    24, 4),
			:rgb888,      define_pixelformat(:packed32, PackedOrder[:xrgb],       :layout_8888,    24, 4),
			:rgbx8888,    define_pixelformat(:packed32, PackedOrder[:rgbx],       :layout_8888,    24, 4),
			:xbgr8888,    define_pixelformat(:packed32, PackedOrder[:xbgr],       :layout_8888,    24, 4),
			:bgr888,      define_pixelformat(:packed32, PackedOrder[:xbgr],       :layout_8888,    24, 4),
			:bgrx8888,    define_pixelformat(:packed32, PackedOrder[:bgrx],       :layout_8888,    24, 4),
			:argb8888,    define_pixelformat(:packed32, PackedOrder[:argb],       :layout_8888,    32, 4),
			:rgba8888,    define_pixelformat(:packed32, PackedOrder[:rgba],       :layout_8888,    32, 4),
			:abgr8888,    define_pixelformat(:packed32, PackedOrder[:abgr],       :layout_8888,    32, 4),
			:bgra8888,    define_pixelformat(:packed32, PackedOrder[:bgra],       :layout_8888,    32, 4),
			:argb2101010, define_pixelformat(:packed32, PackedOrder[:argb],       :layout_2101010, 32, 4),
			:yv12,         define_pixelfourcc('YV12'),
			:iyuv,         define_pixelfourcc('IYUV'),
			:yuy2,         define_pixelfourcc('YUY2'),
			:uyvy,         define_pixelfourcc('UYVY'),
			:yvyu,         define_pixelfourcc('YVYU'),
			:nv12,         define_pixelfourcc('NV12'),
			:nv21,         define_pixelfourcc('NV21'),
			:external_oes, define_pixelfourcc('OES ')
		]
		class PixelFormatEnumPtr < PrimitivePtr; type(PixelFormatEnum); end

		class << self
			def pixelflag(pixel_format)
				(PixelFormatEnum.to_native(pixel_format) >> 28) & 0x0F
			end

			def pixeltype(pixel_format)
				PixelType.from_native((PixelFormatEnum.to_native(pixel_format) >> 24) & 0x0F)
			end

			def pixelorder(pixel_format)
				if pixelformat_indexed?(pixel_format)
					BitmapOrder.from_native((PixelFormatEnum.to_native(pixel_format) >> 20) & 0x0F)
				elsif pixelformat_packed?(pixel_format)
					PackedOrder.from_native((PixelFormatEnum.to_native(pixel_format) >> 20) & 0x0F)
				elsif pixelformat_array?(pixel_format)
					ArrayOrder.from_native((PixelFormatEnum.to_native(pixel_format) >> 20) & 0x0F)
				else
					(PixelFormatEnum.to_native(pixel_format) >> 20) & 0x0F
				end
			end

			def pixellayout(pixel_format)
				PackedLayout.from_native((PixelFormatEnum.to_native(pixel_format) >> 16) & 0x0F)
			end

			def bitsperpixel(pixel_format)
				(PixelFormatEnum.to_native(pixel_format) >> 8) & 0xFF
			end

			def bytes_per_pixel(pixel_format)
				if pixelformat_fourcc?(pixel_format)
					if [:pixelformat_yuy2, :pixelformat_uyvy, :pixelformat_yvyu].include?(pixel_format)
						2
					else
						1
					end
				else
					PixelFormatEnum.to_native(pixel_format) & 0xFF
				end
			end

			def ispixelformat_indexed(pixel_format)
				!pixelformat_fourcc?(pixel_format) &&
					[:index1, :index4, :index8].include?(pixeltype(pixel_format))
			end
			alias pixelformat_indexed? ispixelformat_indexed

			def ispixelformat_packed(pixel_format)
				!pixelformat_fourcc?(pixel_format) &&
					[:packed8, :packed16, :packed32].include?(pixeltype(pixel_format))
			end
			alias pixelformat_packed? ispixelformat_packed

			def ispixelformat_array(pixel_format)
				!pixelformat_fourcc?(pixel_format) &&
					[:arrayu8, :arrayu16, :arrayu32, :arrayf16, :arrayf32].include?(pixeltype(pixel_format))
			end
			alias pixelformat_array? ispixelformat_array

			def ispixelformat_alpha(pixel_format)
				(pixelformat_packed?(pixel_format) || pixelformat_array?(pixel_format)) &&
					[:argb, :rgba, :abgr, :bgra].include?(pixelorder(pixel_format))
			end
			alias pixelformat_alpha? ispixelformat_alpha

			def ispixelformat_fourcc(pixel_format)
				pixel_format != :unknown && pixelflag(pixel_format) != 1
			end
			alias pixelformat_fourcc? ispixelformat_fourcc
		end

		class Color < FFI::Struct
			layout(
				:r, :uint8,
				:g, :uint8,
				:b, :uint8,
				:a, :uint8
			)

			def self.from_rgba(rgba)
				SDL2::Color.from_rgba(rgba).native
			end
		end
		class Palette < FFI::Struct
			layout(
				:ncolors, :int,
				:colors, Color.ptr,
				:version, :uint32,
				:refcount, :int
			)
		end

		class PixelFormat < FFI::Struct
			layout(
				:format, PixelFormatEnum,
				:palette, Palette.ptr,
				:BitsPerPixel, :uint8,
				:BytesPerPixel, :uint8,
				:padding1, :uint8,
				:padding2, :uint8,
				:Rmask, :uint32,
				:Gmask, :uint32,
				:Bmask, :uint32,
				:Amask, :uint32,
				:Rloss, :uint8,
				:Gloss, :uint8,
				:Bloss, :uint8,
				:Aloss, :uint8,
				:Rshift, :uint8,
				:Gshift, :uint8,
				:Bshift, :uint8,
				:Ashift, :uint8,
				:refcount, :int,
				:next, self.ptr
			)
		end

		# class allocformat < FFI::Struct
		# class allocpalette < FFI::Struct
		# class calculategammaramp < FFI::Struct
		# class freeformat < FFI::Struct
		# class freepalette < FFI::Struct
		# class getpixelformatname < FFI::Struct
		# class getrgb < FFI::Struct
		# class getrgba < FFI::Struct
		# class maprgb < FFI::Struct
		# class maprgba < FFI::Struct
		# class maskstopixelformatenum < FFI::Struct
		# class pixelformatenumtomasks < FFI::Struct
		# class setpalettecolors < FFI::Struct
		# class setpixelformatpalette < FFI::Struct
	end
end
