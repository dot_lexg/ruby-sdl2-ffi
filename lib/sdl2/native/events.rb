# frozen_string_literal: true

require_relative 'keyboard'

module SDL2
	# @!group SDL_events.h
	# @api native
	module Native
		EventType = enum FFI::TYPE_UINT32, :EventType, [
			:firstevent, 0,
			:quit, 0x100,
			:app_terminating,
			:app_lowmemory,
			:app_willenterbackground,
			:app_didenterbackground,
			:app_willenterforeground,
			:app_didenterforeground,
			:displayevent, 0x150,
			:windowevent, 0x200,
			:syswmevent,
			:keydown, 0x300,
			:keyup,
			:textediting,
			:textinput,
			:keymapchanged,
			:mousemotion, 0x400,
			:mousebuttondown,
			:mousebuttonup,
			:mousewheel,
			:joyaxismotion, 0x600,
			:joyballmotion,
			:joyhatmotion,
			:joybuttondown,
			:joybuttonup,
			:joydeviceadded,
			:joydeviceremoved,
			:controlleraxismotion, 0x650,
			:controllerbuttondown,
			:controllerbuttonup,
			:controllerdeviceadded,
			:controllerdeviceremoved,
			:controllerdeviceremapped,
			:fingerdown, 0x700,
			:fingerup,
			:fingermotion,
			:dollargesture, 0x800,
			:dollarrecord,
			:multigesture,
			:clipboardupdate, 0x900,
			:dropfile, 0x1000,
			:droptext,
			:dropbegin,
			:dropcomplete,
			:audiodeviceadded, 0x1100,
			:audiodeviceremoved,
			:sensorupdate, 0x1200,
			:render_targets_reset, 0x2000,
			:render_device_reset,
			:userevent, 0x8000,
			:lastevent, 0xFFFF
		]

		WindowEventId = enum FFI::TYPE_UINT8, :WindowEventID, [
			:none,
			:shown,
			:hidden,
			:exposed,
			:moved,
			:resized,
			:size_changed,
			:minimized,
			:maximized,
			:restored,
			:enter,
			:leave,
			:focus_gained,
			:focus_lost,
			:close,
			:take_focus,
			:hit_test
		]

		EventStateQuery = enum :EventStateQuery, [
			:query, -1,
			:disable, 0,
			:ignore, 0,
			:enable, 1
		]
		EventStateResult = enum FFI::TYPE_UINT8, :EventStateResult, [
			:disable, 0,
			:enable, 1
		]

		class Event < FFI::Struct
			# Layout of SDL_CommonEvent because SDL_Event is a tagged union and this is the common subset
			layout(
				type: EventType,
				timestamp: :uint32,
				data: [:char, 48]
			)
			raise unless self.size == 56

			def resolve_type
				case self[:type]
				when :audiodeviceadded, :audiodeviceremoved
					AudioDeviceEvent.new(self.pointer)
				when :controlleraxismotion
					ControllerAxisEvent.new(self.pointer)
				when :controllerbuttondown, :controllerbuttonup
					ControllerButtonEvent.new(self.pointer)
				when :controllerdeviceadded, :controllerdeviceremoved, :controllerdeviceremapped
					ControllerDeviceEvent.new(self.pointer)
				when :dollargesture, :dollarrecord
					DollarGestureEvent.new(self.pointer)
				when :dropfile, :droptext, :dropbegin, :dropcomplete
					DropEvent.new(self.pointer)
				when :joyaxismotion
					JoyAxisEvent.new(self.pointer)
				when :joyballmotion
					JoyBallEvent.new(self.pointer)
				when :joybuttondown, :joybuttonup
					JoyButtonEvent.new(self.pointer)
				when :joydeviceadded, :joydeviceremoved
					JoyDeviceEvent.new(self.pointer)
				when :joyhatmotion
					JoyHatEvent.new(self.pointer)
				when :keydown, :keyup
					KeyboardEvent.new(self.pointer)
				when :mousebuttondown, :mousebuttonup
					MouseButtonEvent.new(self.pointer)
				when :mousemotion
					MouseMotionEvent.new(self.pointer)
				when :mousewheel
					MouseWheelEvent.new(self.pointer)
				when :multigesture
					MultiGestureEvent.new(self.pointer)
				when :quit
					QuitEvent.new(self.pointer)
				when :syswmevent
					SysWMEvent.new(self.pointer)
				when :textediting
					TextEditingEvent.new(self.pointer)
				when :textinput
					TextInputEvent.new(self.pointer)
				when :fingerdown, :fingerup, :fingermotion
					TouchFingerEvent.new(self.pointer)
				when :userevent
					UserEvent.new(self.pointer)
				when :windowevent
					WindowEvent.new(self.pointer)
				when :sensorupdate
					SensorEvent.new(self.pointer)
				when :displayevent
					DisplayEvent.new(self.pointer) # XXX: This is not documented
				else
					# :app_terminating, :app_lowmemory, :app_willenterbackground, :app_didenterbackground, :app_willenterforeground, :app_didenterforeground, :keymapchanged, :clipboardupdate, :render_targets_reset, :render_device_reset
					self
				end
			end

		end

		class AudioDeviceEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :uint32,
				iscapture: :uint8
				# padding1: :uint8,
				# padding2: :uint8,
				# padding3: :uint8
			)
		end

		class ControllerAxisEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :int32,
				axis: :uint8,
				# padding1: :uint8,
				# padding2: :uint8,
				# padding3: :uint8,
				value: :int16
				# padding4: :uint16
			)
		end

		class ControllerButtonEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :int32,
				button: :uint8,
				state: :uint8
				# padding1: :uint8,
				# padding2: :uint8
			)
		end

		class ControllerDeviceEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :int32
			)
		end

		# XXX: was not documented in this category
		class DisplayEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				display: :uint32,
				event: :uint8,
				# padding1: :uint8,
				# padding2: :uint8,
				# padding3: :uint8,
				data1: :int32
			)
		end

		class DollarGestureEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				touch_id: :int64,
				gesture_id: :int64,
				num_fingers: :uint32,
				error: :float,
				x: :float,
				y: :float
			)
		end

		class DropEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				file: :string,
				window_id: :uint32
			)
		end

		class JoyAxisEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :int32,
				axis: :uint8,
				# padding1: :uint8,
				# padding2: :uint8,
				# padding3: :uint8,
				value: :int16
				# padding4: :uint16
			)
		end

		class JoyBallEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :int32,
				ball: :uint8,
				# padding1: :uint8,
				# padding2: :uint8,
				# padding3: :uint8,
				xrel: :int16,
				yrel: :int16
			)
		end

		class JoyButtonEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :int32,
				button: :uint8,
				state: :uint8
				# padding1: :uint8,
				# padding2: :uint8
			)
		end

		class JoyDeviceEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :int32
			)
		end

		class JoyHatEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :int32,
				hat: :uint8,
				value: :uint8
				# padding1: :uint8,
				# padding2: :uint8
			)
		end

		class KeyboardEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				window_id: :uint32,
				state: :uint8,
				repeat: :uint8,
				# padding2: :uint8,
				# padding3: :uint8,
				keysym: Keysym
			)
		end

		# XXX: originally defined in SDL_mouse.h
		MouseButton = enum FFI::TYPE_UINT8, :MouseButton, [
			:left, 1,
			:middle, 2,
			:right, 3,
			:x1, 4,
			:x2, 5
		]
		MouseState = bitmask FFI::TYPE_UINT32, :MouseState, [
			:left, 0,
			:middle, 1,
			:right, 2,
			:x1, 3,
			:x2, 4
		]
		MouseButtonState = enum FFI::TYPE_UINT8, :MouseButtonState, [
			:released, 0,
			:pressed, 1
		]

		MouseWheelDirection = enum FFI::TYPE_UINT32, :MouseWheelDirection, [
			:normal,
			:flipped
		]

		TOUCH_MOUSEID = -1 # XXX: From SDL_touch.h

		class MouseButtonEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				window_id: :uint32,
				which: :uint32,
				button: MouseButton,
				state: MouseButtonState,
				clicks: :uint8,
				# padding1: :uint8,
				x: :int32,
				y: :int32
			)
		end

		class MouseMotionEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				window_id: :uint32,
				which: :uint32,
				state: MouseState,
				x: :int32,
				y: :int32,
				xrel: :int32,
				yrel: :int32
			)
		end

		class MouseWheelEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				window_id: :uint32,
				which: :uint32,
				x: :int32,
				y: :int32,
				direction: MouseWheelDirection
			)
		end

		class MultiGestureEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				touch_id: :int64,
				d_theta: :float,
				d_dist: :float,
				x: :float,
				y: :float,
				num_fingers: :uint16
				# padding: :uint16
			)
		end

		class QuitEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32
			)
		end

		# XXX: was not documented in this category
		class SensorEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				which: :int32,
				data: [:float, 6]
			)
		end

		class SysWMEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				msg: :pointer # Opaque SysWMmsg *
			)
		end

		class TextEditingEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				window_id: :uint32,
				text: [:char, 32],
				start: :int32,
				length: :int32
			)
		end

		class TextInputEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				window_id: :uint32,
				text: [:char, 32]
			)
		end

		class TouchFingerEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				touch_id: :int64,
				finger_id: :int64,
				x: :float,
				y: :float,
				dx: :float,
				dy: :float,
				pressure: :float,
				window_id: :uint32
			)
		end

		class UserEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				window_id: :uint32,
				code: :int32,
				data1: :pointer, # void *
				data2: :pointer  # void *
			)
		end

		class WindowEvent < FFI::Struct
			self.size = 56
			layout(
				type: EventType,
				timestamp: :uint32,
				window_id: :uint32,
				event: WindowEventId,
				# padding1: :uint8,
				# padding2: :uint8,
				# padding3: :uint8,
				data1: :int32,
				data2: :int32
			)
			raise unless offsets[4] == [:data1, 16] # Make sure padding was added for us
		end

		# class Finger < FFI::Struct
		# 		SDL_FingerID id;
		# 		float x;
		# 		float y;
		# 		float pressure;
		# end

		# attach_function :add_event_watch, :SDL_AddEventWatch
		# attach_function :del_event_watch, :SDL_DelEventWatch

		# @!method self.event_state(event_type, state)
		# @param event_type [EventType]
		# @param state [EventStateQuery] :ignore/:disable, :enable, or :query
		# @return [EventStateResult] what it used to be, :disable or :enable
		attach_function :event_state, :SDL_EventState, [EventType, EventStateQuery], EventStateResult

		# attach_function :filter_events, :SDL_FilterEvents
		# attach_function :flush_event, :SDL_FlushEvent
		# attach_function :flush_events, :SDL_FlushEvents
		# attach_function :get_event_filter, :SDL_GetEventFilter
		# attach_function :get_event_state, :SDL_GetEventState
		# attach_function :get_num_touch_devices, :SDL_GetNumTouchDevices
		# attach_function :get_num_touch_fingers, :SDL_GetNumTouchFingers
		# attach_function :get_touch_device, :SDL_GetTouchDevice
		# attach_function :get_touch_finger, :SDL_GetTouchFinger
		# attach_function :has_event, :SDL_HasEvent
		# attach_function :has_events, :SDL_HasEvents
		# attach_function :load_dollar_templates, :SDL_LoadDollarTemplates
		# attach_function :peep_events, :SDL_PeepEvents

		# @!method self.poll_event(event)
		# @param event [out SDL_Event *, nil] the SDL_Event structure to be filled with the next event from the queue, or NULL.
		#   Pops the queue if supplied.
		# @return [0, 1] Returns 1 if there is a pending event or 0 if there are none available.
		attach_function :poll_event, :SDL_PollEvent, [Event.ptr(:out)], :int

		# attach_function :pump_events, :SDL_PumpEvents
		# attach_function :push_event, :SDL_PushEvent
		# attach_function :quit_requested, :SDL_QuitRequested
		# attach_function :record_gesture, :SDL_RecordGesture
		# attach_function :register_events, :SDL_RegisterEvents
		# attach_function :save_all_dollar_templates, :SDL_SaveAllDollarTemplates
		# attach_function :save_dollar_template, :SDL_SaveDollarTemplate
		# attach_function :set_event_filter, :SDL_SetEventFilter
		# attach_function :wait_event, :SDL_WaitEvent
		# attach_function :wait_event_timeout, :SDL_WaitEventTimeout
	end
end
