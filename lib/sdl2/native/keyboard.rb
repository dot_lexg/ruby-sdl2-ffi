# frozen_string_literal: true

require_relative 'scancode'
require_relative 'keycode'

module SDL2
	# @!group SDL_keyboard.h
	# @api native
	module Native
		class Keysym < FFI::Struct
			layout(
				scancode: Scancode,
				sym: Keycode,
				mod: Keymod,
				unused: :uint32
			)
		end

		# TODO: fill in rest of stuff from the category
	end
end
