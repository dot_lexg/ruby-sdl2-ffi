# frozen_string_literal: true

require_relative 'sdl2/display'
require_relative 'sdl2/event'
require_relative 'sdl2/native'
require_relative 'sdl2/renderer'
require_relative 'sdl2/surface'
require_relative 'sdl2/texture'
require_relative 'sdl2/window'
require_relative 'sdl2/wrapper_version'
require_relative 'sdl2/vector'
require_relative 'sdl2/version'

module SDL2
	class << self
		def init(*subsystems)
			subsystems = Native::InitFlags.symbols if subsystems.empty? || subsystems == [:everything]
			Native.check(:init, subsystems)
			at_exit do
				Native.quit
			end
			nil
		end

		def screen_saver
			Native.is_screen_saver_enabled
		end
		alias screen_saver? screen_saver

		def screen_saver=(enabled)
			if enabled
				Native.enable_screen_saver
			else
				Native.disable_screen_saver
			end
		end

		def video_drivers
			count = Native.check(:get_num_video_drivers)
			count.times.map do |i|
				Native.get_video_driver(i)
			end
		end

		def current_video_driver
			Native.get_current_video_driver
		end
	end

	Rect = Struct.new(:x, :y, :w, :h) do
		def self.from_native(native)
			new(native[:x], native[:y], native[:w], native[:h])
		end

		def native
			color = Native::Rect.new
			color[:x] = self.x
			color[:y] = self.y
			color[:w] = self.w
			color[:h] = self.h
			color
		end
	end

	Color = Struct.new(:r, :g, :b, :a) do
		def self.from_native(native)
			new(native[:r], native[:g], native[:b], native[:a])
		end

		def self.from_rgba(rgba)
			if rgba.is_a?(Integer)
				new(
					rgba >> 24 & 0xFF,
					rgba >> 16 & 0xFF,
					rgba >>  8 & 0xFF,
					rgba >>  0 & 0xFF
				)
			elsif rgba.length == 4
				new(*rgba)
			else
				raise ArgumentError, "Must specify color as 0xrrggbbaa or [r, g, b, a] but got #{rgba.inspect}"
			end
		end

		def native
			color = Native::Color.new
			color[:r] = self.r
			color[:g] = self.g
			color[:b] = self.b
			color[:a] = self.a
			color
		end
	end
end
