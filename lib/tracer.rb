# frozen_string_literal: true

require 'delegate'

class Tracer < SimpleDelegator
	class << self
		alias trace new

		def trace_module(mod, stream: $stderr, ignore: [])
			(mod.singleton_methods(false) - ignore).each do |method|
				orig_method = mod.singleton_method(method)
				mod.define_singleton_method(method) do |*args, **kwargs, &block|
					Tracer.print_trace(stream, mod.inspect, method, args, kwargs, block) { orig_method.(*args, **kwargs, &block) }
				end
			end
		end

		def print_trace(stream, ctx, method, args, kwargs, block)
			result = nil
			completed = false
			r0 = Process.clock_gettime(Process::CLOCK_MONOTONIC)
			begin
				result = yield
				completed = true
				result
			ensure
				elapsed = Process.clock_gettime(Process::CLOCK_MONOTONIC) - r0
				result_str = completed ? "=> #{result.inspect}" : "raised #{$!.inspect}"
				args_ary = args.map(&:inspect)
				args_ary.concat(kwargs.map {|k, v| "#{k}: #{v.inspect}" })
				stream.puts "[#{'%9.6f' % elapsed}] #{ctx}: .#{method}(#{args_ary.join(', ')})#{' { ... }' if block} #{result_str}"
			end
		end
	end

	def initialize(object, stream: $stderr, ignore: [])
		super(object)
		@stream = stream
		@ignore = ignore
	end

	def method_missing(method, *args, **kwargs, &block)
		if @ignore.include(method)
			super
		else
			print_trace(@stream, __getobj__.inspect, method, args, kwargs, block) { super }
		end
	end
end
